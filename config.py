class BaseConfig(object):
    """ Base configuration class """
    DB_USER_REGISTER = 'UserRegister'
    DB_ACCESS_TOLKEN = 'access_tolken'
    USER_PROFILE = 'user_profile'
    # DB_SETTING       = 'setting'
    # DB_AWARENESS     = 'awareness'
    # DB_AWARENESS_DETAILS = 'awareness_details'
    # DB_EXERCISE = 'journal_exercise'
    # DB_EXERCISE_JOURNAL_NEW = 'journal_exercise_new'
    # DB_SLEEP = 'journal_sleep'
    # DB_FOOD = 'journal_food'
    # DB_FOOD_NEW = 'journal_food_new'
    # DB_FOOD_NUTRIENTS = 'journal_food_nutrients'
    # DB_GRATITUDE = 'journal_gratitude'
    # DB_REPORTING = 'reporting'
    # DB_REPORTING_EMOTIONAL = 'reporting_emotional'
    # DB_REPORTING_PHYSICAL = 'reporting_physical'
    # DB_MULTIMEDIA = 'multimedia'
    # DB_MULTIMEDIA_USER_CONTENT = 'multimedia_user_content'
    # DB_USER_FEEDBACK='user_feedback'
	# #DB_MEDIA_CATEGORIES = 'media_categories'
    # DB_USER_FAVORITES='user_favorites'
    # DB_MEDIA_CATEGORIES = 'media_categories'
    
    DB_USER = "root"
    DB_PASSWORD = "admin"

    DB_INSTANCE = 'localhost'

    CSRF_ENABLED = True

    CORS_HEADERS = 'Content-Type'
    SQLALCHEMY_TRACK_MODIFICATIONS = True


class DeveloperConfig(BaseConfig):
    """ Developer configuration class """
    DB_NAME = 'mydatabase'
    SECRET_KEY = 'secret_key'

    SQLALCHEMY_DATABASE_URI = 'mysql://{}:{}@{}/{}'.format(BaseConfig.DB_USER, BaseConfig.DB_PASSWORD,
                                                           BaseConfig.DB_INSTANCE, DB_NAME)


class ProductionConfig(BaseConfig):
    """ Production configuration class """
    pass
    
