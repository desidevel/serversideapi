from sqlalchemy import MetaData 
from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy

from flask.ext.cors import CORS

app = Flask(__name__)
cors = CORS(app)
app.config.from_object('config.DeveloperConfig')

db = SQLAlchemy(app)

from controller.views import wesoeco_view
app.register_blueprint(wesoeco_view)


import os
import smtplib
from flask_mail import Mail, Message
from werkzeug.utils import secure_filename

#Added by chaitanya
app.config['MAIL_SERVER']='smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'desidevel@gmail.com'
app.config['MAIL_PASSWORD'] = 'Mirankit7693@'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True

mail = Mail(app)
#msg  = Message(app)

# This is the path to the upload directory
app.config['UPLOAD_FOLDER'] = 'F:\PYTHON_SERVER_HOME\Server Side\wesoeco\UPLOADS'
# These are the extension that we are accepting to be uploaded
app.config['ALLOWED_EXTENSIONS'] = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

