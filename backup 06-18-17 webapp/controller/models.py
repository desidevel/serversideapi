from webapp import db, app


class UserRegister(db.Model):
    __tablename__ = app.config['DB_USER_REGISTER']

    id = db.Column(db.BigInteger, primary_key=True)
    external_email_id = db.Column(db.String(50))
    email_id = db.Column(db.String(50), nullable=False)
    password = db.Column(db.String(50), nullable=False)
    user_name = db.Column(db.String(50), nullable=False)
    wesoeco_tier = db.Column(db.Enum('1', '2', '3'))
    login_app = db.Column(db.String(1))
    creation_time = db.Column(db.DateTime)
    update_time = db.Column(db.DateTime)
    access_tolken = db.relationship('AccessTolken', backref='user_register',
                                    cascade="all, delete, delete-orphan",
                                    lazy='dynamic')
    
class AccessTolken(db.Model):
    __tablename__ = app.config['DB_ACCESS_TOLKEN']

    id = db.Column(db.BigInteger, primary_key=True)
    registration_id = db.Column(db.BigInteger, db.ForeignKey('{}.id'.format(app.config['DB_USER_REGISTER'])))
    access_tolken = db.Column(db.String(100), nullable=False)
    creation_time = db.Column(db.DateTime)
    update_time = db.Column(db.DateTime)


# class Setting(db.Model):
#     __tablename__ = app.config['DB_SETTING']

#     id = db.Column(db.BigInteger, primary_key=True)
#     user_id = db.Column(db.BigInteger, db.ForeignKey('{}.id'.format(app.config['DB_USER_REGISTER'])))
#     type = db.Column(db.Enum('breath', 'aware', 'improve', 'optimize'))
#     start_time = db.Column(db.Integer)
#     end_time = db.Column(db.Integer)
#     interval = db.Column(db.Integer)
#     days_of_week = db.Column(db.String(10))
#     tone = db.Column(db.Integer)
#     toggle = db.Column(db.Boolean)
#     creation_time = db.Column(db.DateTime)
#     update_time = db.Column(db.DateTime)

    
# class ExerciseJournal(db.Model):
#     __tablename__ = app.config['DB_EXERCISE']
    
#     user_id = db.Column(db.BigInteger, primary_key=True)
#     exercise_date = db.Column(db.Date, primary_key=True)
#     exercise_id = db.Column(db.BigInteger, primary_key=True)
#     user_id = db.Column(db.BigInteger, db.ForeignKey('{}.id'.format(app.config['DB_USER_REGISTER'])))
#     exercise_desc = db.Column(db.String(30))
#     exercise_duration = db.Column(db.Integer)
#     calories_burned = db.Column(db.Integer)
#     outdoor_ind = db.Column(db.Enum('yes', 'no'))
    
# # Ankit
# class ExerciseJournalNew(db.Model):
#     __tablename__ = app.config['DB_EXERCISE_JOURNAL_NEW']
    
#     exercise_id = db.Column(db.BigInteger, primary_key=True)
#     user_id = db.Column(db.BigInteger, db.ForeignKey('{}.id'.format(app.config['DB_USER_REGISTER'])))
#     exercise_date = db.Column(db.Date, nullable=False)
#     exercise_desc = db.Column(db.String(30))
#     exercise_duration = db.Column(db.Integer)
#     calories_burned = db.Column(db.Integer)
#     outdoor_ind = db.Column(db.Enum('yes', 'no'))
    
    
# class SleepJournal(db.Model):
#     __tablename__ = app.config['DB_SLEEP']
    
#     user_id = db.Column(db.BigInteger, primary_key=True)
#     sleep_date = db.Column(db.Date, primary_key=True)
#     sleep_id = db.Column(db.BigInteger, primary_key=True)
#     user_id = db.Column(db.BigInteger, db.ForeignKey('{}.id'.format(app.config['DB_USER_REGISTER'])))
#     sleep_start_time = db.Column(db.Integer)
#     sleep_duration = db.Column(db.Integer)
#     sleep_duration_hours = db.Column(db.Integer)
#     sleep_duration_minutes = db.Column(db.Integer)
    
# class FoodJournal(db.Model):
#     __tablename__ = app.config['DB_FOOD']
    
#     user_id = db.Column(db.BigInteger, primary_key=True)
#     food_date = db.Column(db.Date, primary_key=True)
#     food_id = db.Column(db.BigInteger, primary_key=True)
#     user_id = db.Column(db.BigInteger, db.ForeignKey('{}.id'.format(app.config['DB_USER_REGISTER'])))
#     meal_type = db.Column(db.Enum('B', 'L', 'D', 'S', 'W'))
#     food_desc = db.Column(db.String(30))
#     quantity_consumed = db.Column(db.Integer)
#     metric_of_measure = db.Column(db.String(30))
#     calories_consumed = db.Column(db.Integer)
#     organic_ind = db.Column(db.Enum('yes', 'no', 'n/a'))
    
# class FoodJournalNew(db.Model):
#     __tablename__ = app.config['DB_FOOD_NEW']

#     food_id = db.Column(db.BigInteger, primary_key=True)
#     user_id = db.Column(db.BigInteger, primary_key=True)
#     food_date = db.Column(db.Date, primary_key=True)
#     user_id = db.Column(db.BigInteger, db.ForeignKey('{}.id'.format(app.config['DB_USER_REGISTER'])))
#     meal_type = db.Column(db.Enum('B', 'L', 'D', 'S', 'W'))
#     food_desc = db.Column(db.String(30))
#     quantity_consumed = db.Column(db.Integer)
#     metric_of_measure = db.Column(db.String(30))
#     calories_consumed = db.Column(db.Integer)
#     organic_ind = db.Column(db.Enum('yes', 'no', 'n/a'))
#     usda_food_id = db.Column(db.String(20))
    
# class FoodJournalNutrients(db.Model):
#     __tablename__ = app.config['DB_FOOD_NUTRIENTS']

#     food_id = db.Column(db.BigInteger, primary_key=True)
#     usda_nutrient_id = db.Column(db.Integer, primary_key=True)
#     usda_nutrient_name = db.Column(db.String(100))
#     nutrient_value_consumed = db.Column(db.Float)
    
# class GratitudeJournal(db.Model):
#     __tablename__ = app.config['DB_GRATITUDE']
    
#     user_id = db.Column(db.BigInteger, primary_key=True)
#     gratitude_date = db.Column(db.Date, primary_key=True)
#     gratitude_id = db.Column(db.BigInteger, primary_key=True)
#     user_id = db.Column(db.BigInteger, db.ForeignKey('{}.id'.format(app.config['DB_USER_REGISTER'])))
#     gratitude_title = db.Column(db.String(300))
#     journal_image_address = db.Column(db.String(300))
#     journal_text = db.Column(db.String(300))
    

# class Reporting(db.Model):
#     __tablename__ = app.config['DB_REPORTING']

#     id = db.Column(db.BigInteger, primary_key=True)
#     user_id = db.Column(db.BigInteger, db.ForeignKey('{}.id'.format(app.config['DB_USER_REGISTER'])))
#     report_type = db.Column(db.Enum('physical', 'mental', 'emotional', 'energetic'))
#     rating = db.Column(db.Integer)
#     activity = db.Column(db.String(100))
#     creation_time = db.Column(db.DateTime)
#     update_time = db.Column(db.DateTime)

#     reportemotional = db.relationship('ReportingEmotional', backref='reporting',
#                                     cascade="all, delete, delete-orphan",
#                                     lazy='dynamic')
#     reportphysical = db.relationship('ReportingPhysical', backref='reporting',
#                                     cascade="all, delete, delete-orphan",
#                                     lazy='dynamic')



# class ReportingEmotional(db.Model):
#     __tablename__ = app.config['DB_REPORTING_EMOTIONAL']

#     id = db.Column(db.BigInteger, primary_key=True)
#     user_id = db.Column(db.BigInteger, db.ForeignKey('{}.user_id'.format(app.config['DB_REPORTING'])))
    
#     report_emotion_1 = db.Column(db.String(15))
#     report_emotion_2 = db.Column(db.String(15))
#     report_emotion_3 = db.Column(db.String(15))
#     report_emotion_4 = db.Column(db.String(15))
#     report_emotion_5 = db.Column(db.String(15))
#     report_emotion_upto5 = db.Column(db.String(100))
    

# class ReportingPhysical(db.Model):
#     __tablename__ = app.config['DB_REPORTING_PHYSICAL']

#     id = db.Column(db.BigInteger, primary_key=True)
#     user_id = db.Column(db.BigInteger, db.ForeignKey('{}.user_id'.format(app.config['DB_REPORTING'])))
    
#     report_sleep = db.Column(db.Integer)
#     report_powernaps = db.Column(db.Integer)
#     report_vegetables = db.Column(db.Integer)
#     report_wholegrain = db.Column(db.Integer)
#     report_healthyprotein = db.Column(db.Integer)
#     report_fruits = db.Column(db.Integer)
#     report_organic_percent = db.Column(db.Integer)
#     report_water = db.Column(db.Integer)
#     report_healthyoils = db.Column(db.Integer)
#     report_calories = db.Column(db.Integer)
#     report_fav_activity1 = db.Column(db.Integer)
#     report_fav_activity2 = db.Column(db.Integer)
#     report_fav_activity3 = db.Column(db.Integer)
#     report_fav_activity4 = db.Column(db.Integer)
#     report_fav_activity5 = db.Column(db.Integer)
#     report_fav_activity6 = db.Column(db.Integer)
    
# class Awareness(db.Model):
#     __tablename__ = app.config['DB_AWARENESS']

#     id = db.Column(db.BigInteger, primary_key=True)
#     user_id = db.Column(db.BigInteger, db.ForeignKey('{}.id'.format(app.config['DB_USER_REGISTER'])))
#     emotional_rating = db.Column(db.Integer)
#     physical_rating = db.Column(db.Integer)
#     mental_rating = db.Column(db.Integer)
#     energetic_rating = db.Column(db.Integer)
#     creation_time = db.Column(db.DateTime)
#     update_time = db.Column(db.DateTime)
    
#     awarenessdetails = db.relationship('AwarenessDetails', backref='awareness',
#                                     cascade="all, delete, delete-orphan",
#                                     lazy='dynamic')
    
# class AwarenessDetails(db.Model):
#     __tablename__ = app.config['DB_AWARENESS_DETAILS']

#     id = db.Column(db.BigInteger, primary_key=True)
#     user_id = db.Column(db.BigInteger, db.ForeignKey('{}.user_id'.format(app.config['DB_AWARENESS'])))
#     emotional_state_details = db.Column(db.String(50))
#     physical_state_details = db.Column(db.String(50))
#     mental_state_details = db.Column(db.String(50))
#     energetic_state_details = db.Column(db.String(50))
    
    
# class Multimedia(db.Model):
#     __tablename__ = app.config['DB_MULTIMEDIA']

#     media_id = db.Column(db.BigInteger, primary_key=True)
#     category = db.Column(db.String(20))
#     media_type = db.Column(db.String(6))
#     media_file = db.Column(db.String(250))
#     image_file = db.Column(db.String(250))
#     image_title = db.Column(db.String(50))
#     other = db.Column(db.String(25))
#     creation_time = db.Column(db.DateTime)
#     update_time = db.Column(db.DateTime)
    
# class MultimediaUserContent(db.Model):
#     __tablename__ = app.config['DB_MULTIMEDIA_USER_CONTENT']

#     user_id = db.Column(db.BigInteger, primary_key=True)
#     created_date_time = db.Column(db.DateTime, primary_key=True)
#     multimedia_ids = db.Column(db.String(25))
    