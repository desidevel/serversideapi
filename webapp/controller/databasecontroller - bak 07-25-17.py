from webapp.controller import models
from webapp import db
from sqlalchemy import func
import uuid, random
from flask_mail import Mail, Message
import os
import ast


def add_user(email_id, password, user_name, wesoeco_tier=None):
    user = models.UserRegister.query \
           .filter(models.UserRegister.email_id==email_id).first()
    if user:
        # User already exists
        return False
    
    user_registry = models.UserRegister.query \
           .filter(models.UserRegister.external_email_id==email_id).first()
    if not user_registry:
        # User already exists
        user_registry = models.UserRegister()
        
    #user_registry = models.UserRegister()
    user_registry.email_id = email_id
    user_registry.password = password
    user_registry.user_name = user_name
    user_registry.wesoeco_tier = wesoeco_tier

    db.session.add(user_registry)
    db.session.commit()
    return True


def add_external_user(external_email_id, login_app, user_name):
    #user = models.UserRegister.query \
    #       .filter((models.UserRegister.external_email_id==external_email_id) | (models.UserRegister.email_id==external_email_id))
        
    user = models.UserRegister.query \
           .filter(models.UserRegister.external_email_id==external_email_id).first()
    if user:
        # User already exists
        print "111111111111111111111111111111111111111111111111"
        return False
          
    external_user_registry = models.UserRegister.query \
           .filter(models.UserRegister.email_id==external_email_id).first()
    if not external_user_registry:
        # User already exists
        print "2222222222222222222222222222222222222222222222222"
        external_user_registry = models.UserRegister()
        
    print "Email Id NOT found for external user registry"
    external_user_registry.external_email_id = external_email_id
    external_user_registry.user_name = user_name
    external_user_registry.login_app = login_app
    db.session.add(external_user_registry)
    db.session.commit()
    return True


def login(email_id, password):
    user = models.UserRegister.query \
           .filter(models.UserRegister.email_id==email_id) \
           .filter(models.UserRegister.password==password).first()
    if not user:
        return None

    access_tolken = str(uuid.uuid4().int)
    access_tolken_entry = models.AccessTolken()
    access_tolken_entry.registration_id = user.id
    access_tolken_entry.access_tolken = access_tolken

    db.session.add(access_tolken_entry)
    db.session.commit()

    return access_tolken

def external_user_login(external_email_id):
    user = models.UserRegister.query \
           .filter(models.UserRegister.external_email_id==external_email_id).first()
    if not user:
        # User already exists
        print "44444444444444444444444444444444444444444444444444444444"
        return None

    print user
    print "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"
    access_tolken = str(uuid.uuid4().int)
    access_tolken_entry = models.AccessTolken()
    access_tolken_entry.registration_id = user.id
    access_tolken_entry.access_tolken = access_tolken

    db.session.add(access_tolken_entry)
    db.session.commit()

    return access_tolken

"""
def external_user_login_check(external_user_id):
    user = models.UserRegister.query \
           .filter(models.UserRegister.external_user_id==external_user_id).first()
    if not user:
        return None;

    access_tolken = str(uuid.uuid4().int)
    access_tolken_entry = models.AccessTolken()
    access_tolken_entry.registration_id = user.id
    access_tolken_entry.access_tolken = access_tolken

    db.session.add(access_tolken_entry)
    db.session.commit()

    return access_tolken
"""

def forgotpassword(email_id):
	email = models.UserRegister.query \
           .filter(models.UserRegister.email_id==email_id).first()
	print(email_id)

	if not email:
    	    return None
	try:
		#Random Password Generator
		alphabet = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		pw_length = 8
		mypw = ""

		for i in range(pw_length):
    			next_index = random.randrange(len(alphabet))
    			mypw = mypw + alphabet[next_index]
		pswd_check = models.UserRegister.query \
				.filter(models.UserRegister.email_id==email_id).first()
		pswd_check.password = mypw
		db.session.commit()

		print(mypw)
		
		#Mail functionality Starts Here
		msg_sent='Mail_Sent'
		msg = Message('Waliap Reset Password Notification ', sender = 'test.adstockglobal@gmail.com', recipients = ['brkulkarni89@gmail.com']) 
    		msg.body = 'Hello ,'+email_id+ '  ,\n  USER Name has requested to  generate new password ,  the generated  password mentioned below  \n ********\n ' +mypw+ '\n**************' 
		mail.send(msg)
		return msg_sent

	except Exception as e:
		return str(e)


def changepassword(access_tolken, current_password, new_password):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()

    if not access_entry:
        return False

    user = access_entry.user_register
    user_id = user.id
    password_entry = models.UserRegister.query.filter(models.UserRegister.user_id==user_id).first()
    
    if password_entry.password != current_password:
        print "current password is incorrect"
        return False
    password_entry.password = new_password
    db.session.add(password_entry)
    db.session.commit()
    return True

    
def is_access_tolken_valid(access_tolken):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()

    if not access_entry:
        return False
    return True

    
def update_setting(setting_type, toggle, access_tolken, start_time=None, end_time=None,
                   interval=None, days_of_week=None, tone=None):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()

    if not access_entry:
        return False

    user = access_entry.user_register
    user_id = user.id
    setting_entry = models.Setting.query.filter(models.Setting.type==setting_type) \
                        .filter(models.Setting.user_id==user_id).first()
    if not setting_entry:
        setting_entry = models.Setting()
    setting_entry.user_id = user_id
    setting_entry.type = setting_type
    setting_entry.start_time = start_time
    setting_entry.end_time = end_time
    setting_entry.interval = interval
    setting_entry.days_of_week = days_of_week
    setting_entry.tone = tone
    setting_entry.toggle = toggle
    db.session.add(setting_entry)
    db.session.commit()
    return True


def get_setting(access_tolken, setting_type):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()

    if not access_entry:
        None

    user = access_entry.user_register
    user_id = user.id
    setting_entry = models.Setting.query.filter(models.Setting.type==setting_type) \
                        .filter(models.Setting.user_id==user_id).first()
    print setting_entry
    return setting_entry


def get_awareness(access_tolken, start_date, end_date):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()

    if not access_entry:
        None

    user = access_entry.user_register
    user_id = user.id

    awareness_entry = db.session.query(func.date(models.Awareness.creation_time).label('creation_date_grouped'),
        func.avg(models.Awareness.physical_rating).label('physical_rating_avg'), 
        func.avg(models.Awareness.emotional_rating).label('emotional_rating_avg'),
        func.avg(models.Awareness.mental_rating).label('mental_rating_avg'),
        func.avg(models.Awareness.energetic_rating).label('energetic_rating_avg')).filter(models.Awareness.user_id == user_id) \
            .filter(models.Awareness.creation_time >= start_date) \
            .filter(models.Awareness.creation_time <= end_date) \
            .group_by(func.date(models.Awareness.creation_time)).all()
            
    if not awareness_entry:
        return False
    
    print awareness_entry
    return awareness_entry

#Rating.query.with_entities(func.avg(Rating.field2).label('average')).filter(Rating.url == url_string.netloc)
#awareness_entry = db.sessions.query(db.func.avg(models.Awareness.physical_rating).label('physical_rating_avg')).filter(models.Awareness.user_id == user_id) \
#.filter(models.Awareness.creation_time >= start_date) \
#.filter(models.Awareness.creation_time <= end_date) \
#.group_by(models.Awareness.creation_time).all()



#select count(*), user_id, date(convert_tz(creation_time,'+00:00','-8:00')), avg(emotional_rating), avg(physical_rating), avg(mental_rating), avg(energetic_rating) 
# FROM awareness 
# WHERE
#   (date(convert_tz(creation_time,'+00:00','-8:00')) >= '2017-01-01') and
#   (date(convert_tz(creation_time,'+00:00','-8:00')) <= '2017-02-15')
# GROUP BY 
# user_id, date(convert_tz(creation_time,'+00:00','-8:00'));

#select DATE(creation_time), avg(physical_rating), avg(emotional_rating), avg(mental_rating), avg(energetic_rating) from awareness where user_id = 1 and creation_time #between '2017-01-07' and '2017-02-09' group by DATE(creation_time);

            
def get_user_from_access_tolken(access_tolken):
    access_tolken_entry = models.AccessTolken.query \
                          .filter(models.AccessTolken.access_tolken==access_tolken).first()

    return access_tolken_entry.registration_id


def update_awareness(access_tolken, emotional_rating, physical_rating, mental_rating, energetic_rating):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()

    if not access_entry:
        return False

    user = access_entry.user_register
    user_id = user.id
    awareness_entry = models.Awareness()
    
    awareness_entry.user_id = user_id
    awareness_entry.emotional_rating = emotional_rating
    awareness_entry.physical_rating = physical_rating
    awareness_entry.mental_rating = mental_rating
    awareness_entry.energetic_rating = energetic_rating
    db.session.add(awareness_entry)
    db.session.commit()
    return True


def update_awareness_details(access_tolken, emotional_state_details, physical_state_details, mental_state_details, energetic_state_details):
    
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()
    if not access_entry:
        return False
    user = access_entry.user_register
    user_id = user.id
    print user_id
    
    report_entry = models.Awareness.query.order_by(models.Awareness.id.desc()) \
                        .filter(models.Awareness.user_id==user_id).first()
                    
    if not report_entry:
        return False
    report = report_entry.id
    id = report
    
    report_entry_details = models.AwarenessDetails.query.filter(models.AwarenessDetails.id==id) \
                        .filter(models.AwarenessDetails.user_id==user_id).first()
    if not report_entry_details:
        report_entry_details = models.AwarenessDetails()
    
    report_entry_details.id = id
    report_entry_details.user_id = user_id
    report_entry_details.emotional_state_details = emotional_state_details
    report_entry_details.physical_state_details = physical_state_details
    report_entry_details.mental_state_details = mental_state_details
    report_entry_details.energetic_state_details = energetic_state_details
    db.session.add(report_entry_details)
    db.session.commit()
    return True


def get_user_media(access_tolken):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()
        
    if not access_entry:
        return False
    
    user = access_entry.user_register
    user_id = user.id
    user_media_result = models.MultimediaUserContent.query.order_by(models.MultimediaUserContent.created_date_time.desc()) \
                            .filter(models.MultimediaUserContent.user_id==user_id).first()

    if not user_media_result:
        print "User Media not Available, ERROR ===>>>"
        return False
    print user_media_result
    return user_media_result
    
def get_multimedia(media_id):
    
    print "XXX CONTROLLER CODE XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    print id
    multimedia_entry = models.Multimedia.query.filter(models.Multimedia.media_id==media_id).first()
                          
    
    if not multimedia_entry:
        return False
    
    print "GET MultiMedia is Successful"
    print multimedia_entry
    return multimedia_entry
    
    
def add_multimedia(media_type, media_file, image_file, image_title):
    
    media_details = models.Multimedia()
    media_details.media_type = media_type
    media_details.media_file = media_file
    media_details.image_file = image_file
    media_details.image_title = image_title
    db.session.add(media_details)
    db.session.commit()
    return True
    
    
#Ankits code added on June 17th, 2017
def get_user_multimedia(access_tolken):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()
        
    if not access_entry:
        return False
    
    user = access_entry.user_register
    user_id = user.id
    
    get_only = "no"
    
    media_entry = models.MultimediaUserContent.query.order_by(models.MultimediaUserContent.created_date_time.desc()) \
                    .filter(models.MultimediaUserContent.favorites == get_only) \
                    .filter(models.MultimediaUserContent.user_id == user_id).first()

    if not media_entry:
        return False
    
    print media_entry    
    return media_entry


def get_multimedia_content(access_tolken,media_id):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()
        
    if not access_entry:
        return False
    
    user = access_entry.user_register
    user_id = user.id
    print 'media id==>', media_id
    
    media_entry_content = models.Multimedia.query.filter(models.Multimedia.media_id == media_id).all()
                     
    if not media_entry_content:
        return False
    
    print media_entry_content    
    return media_entry_content


def add_to_fav(access_tolken, favorite, media_id):
    get_all_media_ids = []
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()
        
    if not access_entry:
        return False
    
    user = access_entry.user_register
    user_id = user.id
    print 'media id==>', media_id
    get_only = "yes"
    
    media_entry = models.MultimediaUserContent.query.filter(models.MultimediaUserContent.favorites==get_only) \
                    .filter(models.MultimediaUserContent.user_id==user_id).first()
    get_media_id_only = models.MultimediaUserContent.query.filter(models.MultimediaUserContent.favorites==get_only) \
                    .filter(models.MultimediaUserContent.user_id==user_id).first()
    
    get_all_media_ids.append(get_media_id_only.multimedia_ids)

   
    print 'getting all media for add to fav ==>>', get_all_media_ids
    if media_entry is None:
        media_entry = models.MultimediaUserContent()
        mediantry.user_id = user_id
    
    get_all_media_ids.append(media_id)
    all_media_ids = map(str, get_all_media_ids)

    print "All media ids==>", get_all_media_ids
    #using lambda reduce function to remove duplicate value and by the help of frozenset we return list of unique data
    mediaID=list(frozenset(reduce((lambda a, b: a+b), all_media_ids)))

    media_entry.multimedia_ids  = ','.join(mediaID).strip(',')
    media_entry.favorites = "yes"
    db.on.add(media_entry)
    db.session.commit()
    print media_entry
    return True


def get_multimedia_favorite_content(access_tolken ):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()
        
    if not access_entry:
        return False
    
    user = access_entry.user_register
    user_id = user.id
    # print 'fav media id==>', media_id
    get_only = "yes"

    media_entry_content = models.MultimediaUserContent.query.order_by(models.MultimediaUserContent.created_date_time.desc()) \
                    .filter(models.MultimediaUserContent.favorites == get_only) \
                    .filter(models.MultimediaUserContent.user_id == user_id).first()
    
    
    if not media_entry_content:
        return False
    
    print media_entry_content    
    return media_entry_content

def remove_fav(access_tolken, media_id):
    get_all_media_ids = []
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()
        
    if not access_entry:
        return False
    
    user = access_entry.user_register
    user_id = user.id
    print 'media id==>', media_id
    get_only = "yes"
    
    media_entry = models.MultimediaUserContent.query.filter(models.MultimediaUserContent.favorites==get_only) \
                    .filter(models.MultimediaUserContent.user_id==user_id).first()
    get_media_id_only = models.MultimediaUserContent.query.filter(models.MultimediaUserContent.favorites==get_only) \
                    .filter(models.MultimediaUserContent.user_id==user_id).first()
    

    get_all_media_ids.append(get_media_id_only.multimedia_ids)

    if media_entry is None:
        media_entry = models.MultimediaUserContent()
        media_entry.user_id = user_id
    
    get_all_media_ids.append(media_id)
    all_media_ids = map(str, get_all_media_ids)
    
    print "MEDIA ID ==> ", media_id
    print 'all_media_ids == >', all_media_ids.pop()

    remove_quotes =[ast.literal_eval(x) for x in all_media_ids]
    get_media_without_quote = []
    for i in remove_quotes:
        if isinstance(i, tuple)== True:
            for j in i:
                get_media_without_quote.append(j)

    print "Its an eye==>", get_media_without_quote

    if media_id in get_media_without_quote:
        get_media_without_quote.remove(media_id)
        print "After removing from fav media ==>", get_media_without_quote

    remove_id = map(str, get_media_without_quote)
    #using lambda reduce function to remove duplicate value and by the help of frozenset we return list of unique data
    
    media_entry.multimedia_ids  = ','.join(remove_id).strip(',')
    media_entry.favorites = "yes"
    db.session.add(media_entry)
    db.session.commit()
    print media_entry
    return True

#<<<<======== EXERCISE MODULE(s) NEW, STARTS HERE ========>>>>
                #(Last Updated MAY 2017)

#LAST UPDATED on 4/6/2017 & REVIEWED on 5/23/17, GET EXERCISE. This logic retrieves ALL Exercise records that for USERID and Date
def get_exercise(access_tolken, exercise_date):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()

    if not access_entry:
        None

    user = access_entry.user_register
    user_id = user.id
    
    exercise_entry = models.ExerciseJournalNew.query.filter(models.ExerciseJournalNew.exercise_date==exercise_date) \
                      .filter(models.ExerciseJournalNew.user_id==user_id).all()
    
    if not exercise_entry:
        return False
    
    print exercise_entry
    return exercise_entry

#Added on 5/25/17 by Ankit, GET EXERCISE TYPE. The logic retrieves and display Users most recent exercise descriptins for ease of User data entry.
def get_exercise_type(access_tolken):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()

    if not access_entry:
        None

    user = access_entry.user_register
    user_id = user.id
    
    #exercise_types = models.ExerciseJournalNew.query.filter(models.ExerciseJournalNew.exercise_date<=exercise_date) \
    #                  .filter(models.ExerciseJournalNew.user_id==user_id) \
    #                  .group_by(models.ExerciseJournalNew.exercise_desc).distinct()
    exercise_types = models.ExerciseJournalNew.query.filter(models.ExerciseJournalNew.user_id==user_id) \
                      .group_by(models.ExerciseJournalNew.exercise_desc).distinct()

    if not exercise_types:
        return False
    
    print exercise_types    
    return exercise_types     


#Added on 5/24/17 by Rajni - UPDATE EXERCISE NEW 
def update_exercise(access_tolken, exercise_date, exercise_desc, exercise_duration, calories_burned, outdoor_ind, exercise_id):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()
    if not access_entry:
        return False
    
    user = access_entry.user_register
    user_id = user.id
    
    exercise_entry = models.ExerciseJournalNew.query.filter(models.ExerciseJournalNew.exercise_date==exercise_date) \
                      .filter(models.ExerciseJournalNew.exercise_id==exercise_id) \
                      .filter(models.ExerciseJournalNew.user_id==user_id).first()
            
    if exercise_entry is None:
        exercise_entry = models.ExerciseJournalNew()
        exercise_entry.user_id = user_id
        
    exercise_entry.user_id = user_id
    exercise_entry.exercise_date = exercise_date
    exercise_entry.exercise_desc = exercise_desc
    exercise_entry.exercise_duration = exercise_duration
    exercise_entry.calories_burned = calories_burned
    exercise_entry.outdoor_ind = outdoor_ind
    db.session.add(exercise_entry)
    db.session.commit()
    print exercise_entry
    return True


# Added on 5/23/17 - DELETE EXERCISE NEW
def delete_exercise_new(access_tolken, exercise_date, exercise_id):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
        .filter(models.AccessTolken.access_tolken == access_tolken).first()
    if not access_entry:
        return False

    user = access_entry.user_register
    user_id = user.id

    exercise_new_entry = models.ExerciseJournalNew.query.filter(models.ExerciseJournalNew.exercise_date == exercise_date) \
        .filter(models.ExerciseJournalNew.exercise_id == exercise_id) \
        .filter(models.ExerciseJournalNew.user_id == user_id).first()

    if not exercise_new_entry:
        return False
    
    print exercise_new_entry
    print 'Delete Exercise New reached'

    exercise_new_entry.user_id = user_id
    exercise_new_entry.exercise_date = exercise_date
    exercise_new_entry.exercise_id = exercise_id
    print exercise_new_entry
    db.session.delete(exercise_new_entry)
    db.session.commit()
    return True



#UPDATED CODE on 4/4/2017
def get_sleep(access_tolken, sleep_date):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()

    if not access_entry:
        None

    user = access_entry.user_register
    user_id = user.id
    
    sleep_entry = models.SleepJournal.query.filter(models.SleepJournal.sleep_date==sleep_date) \
                      .filter(models.SleepJournal.user_id==user_id).all()
            
    print sleep_entry
    return sleep_entry


def update_sleep(access_tolken, sleep_date, sleep_id, sleep_start_time, sleep_duration_hours, sleep_duration_minutes):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()
    if not access_entry:
        return False
    
    user = access_entry.user_register
    user_id = user.id
    
    sleep_entry = models.SleepJournal.query.filter(models.SleepJournal.sleep_date==sleep_date) \
                      .filter(models.SleepJournal.sleep_id==sleep_id) \
                      .filter(models.SleepJournal.user_id==user_id).first()
            
    if not sleep_entry:
        sleep_entry = models.SleepJournal()
        sleep_entry.user_id = user_id
        sleep_entry.sleep_date = sleep_date
        sleep_entry.sleep_id = sleep_id
    
    sleep_entry.user_id = user_id
    sleep_entry.sleep_date = sleep_date
    sleep_entry.sleep_id = sleep_id
    sleep_entry.sleep_start_time = sleep_start_time
    sleep_entry.sleep_duration_hours = sleep_duration_hours
    sleep_entry.sleep_duration_minutes = sleep_duration_minutes
    db.session.add(sleep_entry)
    db.session.commit()
    print sleep_entry
    return True


def delete_sleep(access_tolken, sleep_date, sleep_id):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()
    if not access_entry:
        return False
    
    user = access_entry.user_register
    user_id = user.id
    
    sleep_entry = models.SleepJournal.query.filter(models.SleepJournal.sleep_date==sleep_date) \
                      .filter(models.SleepJournal.sleep_id==sleep_id) \
                      .filter(models.SleepJournal.user_id==user_id).first()
            
    if not sleep_entry:
        return False
        
    sleep_entry.user_id = user_id
    sleep_entry.sleep_date = sleep_date
    sleep_entry.sleep_id = sleep_id
    print sleep_entry
    db.session.delete(sleep_entry)
    db.session.commit()
    return True


#UPDATED CODE on 4/6/2017, to retrieve all Food records that for USERID and Date
def get_food(access_tolken, food_date):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()

    if not access_entry:
        None

    user = access_entry.user_register
    user_id = user.id
    
    food_entry = models.FoodJournal.query.filter(models.FoodJournal.food_date==food_date) \
                      .filter(models.FoodJournal.user_id==user_id).all()
    
    if not food_entry:
        return False
    
    print "I am HERE FOOD .. retrieved "
    print food_entry
    return food_entry


def update_food(access_tolken, food_date, food_id, meal_type, food_desc, quantity_consumed, metric_of_measure, calories_consumed, organic_ind):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()
    if not access_entry:
        return False
    
    user = access_entry.user_register
    user_id = user.id
    
    food_entry = models.FoodJournal.query.filter(models.FoodJournal.food_date==food_date) \
                      .filter(models.FoodJournal.food_id==food_id) \
                      .filter(models.FoodJournal.user_id==user_id).first()
            
    if not food_entry:
        food_entry = models.FoodJournal()
        food_entry.user_id = user_id
        food_entry.food_date = food_date
        food_entry.food_id = food_id
    
    food_entry.user_id = user_id
    food_entry.food_date = food_date
    food_entry.food_id = food_id
    food_entry.meal_type = meal_type
    food_entry.food_desc = food_desc
    food_entry.quantity_consumed = quantity_consumed
    food_entry.metric_of_measure = metric_of_measure
    food_entry.calories_consumed = calories_consumed
    food_entry.organic_ind = organic_ind

    db.session.add(food_entry)
    db.session.commit()
    print food_entry
    return True

def delete_food(access_tolken, food_date, food_id):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()
    if not access_entry:
        return False
    
    user = access_entry.user_register
    user_id = user.id
    
    food_entry = models.FoodJournal.query.filter(models.FoodJournal.food_date==food_date) \
                      .filter(models.FoodJournal.food_id==food_id) \
                      .filter(models.FoodJournal.user_id==user_id).first()
            
    if not food_entry:
        return False
        
    food_entry.user_id = user_id
    food_entry.food_date = food_date
    food_entry.food_id = food_id
    print food_entry
    db.session.delete(food_entry)
    db.session.commit()
    return True

# 5/12/17 Kulkarni Add Food(USDA Integration) to new Table
def add_food_new(access_tolken, food_date, meal_type, food_desc, quantity_consumed, metric_of_measure,
                calories_consumed, organic_ind, usda_food_id):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
        .filter(models.AccessTolken.access_tolken == access_tolken).first()
    if not access_entry:
        return False

    user = access_entry.user_register
    user_id = user.id
    food_entry = models.FoodJournalNew()

    if  food_entry:
        food_entry.user_id = user_id
        food_entry.food_date = food_date
        food_entry.meal_type = meal_type
        food_entry.food_desc = food_desc
        food_entry.quantity_consumed = quantity_consumed
        food_entry.metric_of_measure = metric_of_measure
        food_entry.calories_consumed = calories_consumed
        food_entry.organic_ind = organic_ind
        food_entry.usda_food_id = usda_food_id

    db.session.add(food_entry)
    db.session.commit()
    return True

# 5/12/17 Rajni - Combined both Add and Update Food functionalities (USDA Integration) code integrates to new Table
def update_food_new(access_tolken, food_date, food_id, meal_type, food_desc, quantity_consumed, metric_of_measure, calories_consumed, organic_ind, usda_food_id):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()
    if not access_entry:
        return False
    
    user = access_entry.user_register
    user_id = user.id
    food_entry = models.FoodJournalNew.query.filter(models.FoodJournalNew.food_date==food_date) \
                      .filter(models.FoodJournalNew.food_id==food_id) \
                      .filter(models.FoodJournalNew.user_id==user_id).first()
            
    if not food_entry:
        food_entry = models.FoodJournalNew()
        #food_entry.user_id = user_id
        #food_entry.food_date = food_date
        food_entry.food_id = food_id
    
    food_entry.user_id = user_id
    food_entry.food_date = food_date
    food_entry.meal_type = meal_type
    food_entry.food_desc = food_desc
    food_entry.quantity_consumed = quantity_consumed
    food_entry.metric_of_measure = metric_of_measure
    food_entry.calories_consumed = calories_consumed
    food_entry.organic_ind = organic_ind
    food_entry.usda_food_id = usda_food_id

    db.session.add(food_entry)
    db.session.commit()
    print food_entry
    return food_entry



def update_foodnutrients_consumed(access_tolken, food_id, nutrientResultList):
    food_id_temp = food_id
    print nutrientResultList
    print food_id_temp
    
    if food_id == 0 or food_id == None:
        access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()
        if not access_entry:
            return False
    
        user = access_entry.user_register
        user_id = user.id

        food_entry = models.FoodJournalNew.query.order_by(models.FoodJournalNew.food_id.desc()) \
                    .filter(models.FoodJournalNew.user_id==user_id).first()
    
        if not food_entry:
            return False
        
        food_id_temp = food_entry.food_id
    
    print 'Food ID is being displayed'
    print food_id_temp
    
    #delete existing nutrient record, in case record is being updated with a new food
    nutrient_entry = models.FoodJournalNutrients.query.filter(models.FoodJournalNutrients.food_id==food_id_temp).delete()
    if not nutrient_entry:
        print 'There are no Nutrients Records to Delete'
    
    for data in range(len(nutrientResultList)):      
        nutrition_dict = nutrientResultList[data]
        print nutrition_dict['usda_nutrient_id'] , nutrition_dict['usda_nutrient_name'] , nutrition_dict['nutrient_value_consumed']
        
        nutrient_entry = models.FoodJournalNutrients()
        nutrient_entry.food_id = food_id_temp
        nutrient_entry.usda_nutrient_id = nutrition_dict['usda_nutrient_id']
        nutrient_entry.usda_nutrient_name = nutrition_dict['usda_nutrient_name']
        nutrient_entry.nutrient_value_consumed = nutrition_dict['nutrient_value_consumed']
        db.session.add(nutrient_entry)
    
    db.session.commit()
    print nutrient_entry    
    return True

# 5/12/17 Kulkarni Get Food(USDA Integration) from new Table
def get_food_new(access_tolken, food_date):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
        .filter(models.AccessTolken.access_tolken == access_tolken).first()

    if not access_entry:
        None

    user = access_entry.user_register
    user_id = user.id

    food_entry = models.FoodJournalNew.query.filter(models.FoodJournalNew.food_date == food_date) \
        .filter(models.FoodJournalNew.user_id == user_id).all()

    if not food_entry:
        return False

    print food_entry
    return food_entry

# 5/12/17 Rajni Delete Food(USDA Integration) from new Table
def delete_food_new(access_tolken, food_date, food_id):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
        .filter(models.AccessTolken.access_tolken == access_tolken).first()
    if not access_entry:
        return False

    user = access_entry.user_register
    user_id = user.id

    delete_nutrient = models.FoodJournalNutrients.query.filter(models.FoodJournalNutrients.food_id == food_id).delete()
    
    food_new_entry = models.FoodJournalNew.query.filter(models.FoodJournalNew.food_date == food_date) \
        .filter(models.FoodJournalNew.food_id == food_id) \
        .filter(models.FoodJournalNew.user_id == user_id).first()

    if not food_new_entry:
        return False
    
    print food_new_entry
    print 'delete Food New reached'

    food_new_entry.user_id = user_id
    food_new_entry.food_date = food_date
    food_new_entry.food_id = food_id
    print food_new_entry
    db.session.delete(food_new_entry)
    db.session.commit()
    return True


def get_gratitude(access_tolken, gratitude_date):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
        .filter(models.AccessTolken.access_tolken == access_tolken).first()

    if not access_entry:
        None

    user = access_entry.user_register
    user_id = user.id

    gratitude_entry = models.GratitudeJournal.query.filter(models.GratitudeJournal.gratitude_date == gratitude_date) \
        .filter(models.GratitudeJournal.user_id==user_id).all()

    return gratitude_entry

def add_gratitude(access_tolken,gratitude_date,gratitude_title,journal_image_address,journal_text):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
        .filter(models.AccessTolken.access_tolken == access_tolken).first()
    if not access_entry:
        return False

    user = access_entry.user_register
    user_id = user.id
    gratitude_entry = models.GratitudeJournal()

    if gratitude_entry:
        gratitude_entry.user_id = user_id
        gratitude_entry.gratitude_date = gratitude_date
        gratitude_entry.gratitude_title = gratitude_title
        gratitude_entry.journal_image_address = journal_image_address
        gratitude_entry.journal_text= journal_text

    db.session.add(gratitude_entry)
    db.session.commit()
    return True


def update_gratitude(access_tolken, gratitude_id, gratitude_date, gratitude_title, journal_image_address, journal_text, serverimagePath):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
        .filter(models.AccessTolken.access_tolken == access_tolken).first()
    if not access_entry:
        return False

    user = access_entry.user_register
    user_id = user.id

    gratitude_entry = models.GratitudeJournal.query.filter(models.GratitudeJournal.gratitude_date == gratitude_date) \
        .filter(models.GratitudeJournal.gratitude_id == gratitude_id) \
        .filter(models.GratitudeJournal.user_id == user_id).first()

    if not gratitude_entry:
        gratitude_entry = models.GratitudeJournal()
        gratitude_entry.user_id = user_id
        gratitude_entry.gratitude_date = gratitude_date
        gratitude_entry.gratitude_id = gratitude_id
    
    if gratitude_entry:
        if gratitude_entry.journal_image_address != serverimagePath:
            if os.path.exists(serverimagePath):
                print 'SERVERIMAGEPATHPath'
                print serverimagePath
                os.remove(serverimagePath)
        
    gratitude_entry.user_id = user_id
    gratitude_entry.gratitude_date = gratitude_date
    gratitude_entry.gratitude_id = gratitude_id
    gratitude_entry.gratitude_title = gratitude_title
    gratitude_entry.journal_image_address = journal_image_address
    gratitude_entry.journal_text = journal_text
    db.session.add(gratitude_entry)
    db.session.commit()
    return True


def delete_gratitude(access_tolken, gratitude_date, gratitude_id):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
        .filter(models.AccessTolken.access_tolken == access_tolken).first()
    if not access_entry:
        return False

    user = access_entry.user_register
    user_id = user.id

    gratitude_entry = models.GratitudeJournal.query.filter(models.GratitudeJournal.gratitude_date == gratitude_date) \
        .filter(models.GratitudeJournal.gratitude_id == gratitude_id) \
        .filter(models.GratitudeJournal.user_id == user_id).first()

    if not gratitude_entry:
        return False
    
    print gratitude_entry
    print 'delete reached'
    print gratitude_entry.journal_image_address
    if os.path.exists(gratitude_entry.journal_image_address):
        os.remove(gratitude_entry.journal_image_address)

    gratitude_entry.user_id = user_id
    gratitude_entry.gratitude_date = gratitude_date
    gratitude_entry.gratitude_id = gratitude_id
    print gratitude_entry
    db.session.delete(gratitude_entry)
    db.session.commit()
    return True


def update_feedback(access_tolken, feedback):
    
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()
    
    if not access_entry:
        return False
    
    user = access_entry.user_register
    user_id = user.id
    print user_id
    
    get_feedback = models.user_feedback.query.filter(models.user_feedback.feedback) \
                    .filter(models.user_feedback.user_id==user_id).first()
    
    if get_feedback is None:
        get_feedback = models.user_feedback()
        get_feedback.user_id = user_id
    
    get_feedback.feedback  = feedback
  
    db.session.add(get_feedback)
    db.session.commit()
    print get_feedback
    return True


def update_favorite(access_tolken, fav_ids):
    data = []
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()
    
    print access_tolken
    print  "User favourites ids: " , fav_ids
    if not access_entry:
        return False
    
    user = access_entry.user_register
    user_id = user.id
    print user_id

    get_fav = models.user_favorites.query.filter(models.user_favorites.fav_ids) \
                    .filter(models.user_favorites.user_id==user_id).first()

    if get_fav is None:
        get_fav = models.user_favorites()
        get_fav.user_id = user_id
    
    for i in fav_ids:
        data.append(i)
    print "Here is ids: ", data
    new_data = ','.join(data)
    print new_data
    get_fav.fav_ids  = ''.join(new_data)

  
    db.session.add(get_fav)
    db.session.commit()
    print get_fav
    return True




def update_gratitude(access_tolken, gratitude_id, gratitude_date, gratitude_title, journal_image_address, journal_text, serverimagePath):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
        .filter(models.AccessTolken.access_tolken == access_tolken).first()
    if not access_entry:
        return False

    user = access_entry.user_register
    user_id = user.id


#ANKITS CODE STARTS HERE 7/23/17

import pandas as pd
import userContentGenerator 
rating_data = []
import time
import views

#final_rating = []
#def get_all_ratings(access_tolken, start_date, end_date):
#    access_entry = models.AccessTolken.query.join(models.UserRegister) \
#                    .filter(models.AccessTolken.access_tolken == access_tolken).first()
#    
#    if not access_entry:
#        return False
    
#	user = access_entry.user_register
#    user_id = user.id
	
#    awareness_entry = db.session.query(models.Awareness.creation_time, 
#        (models.Awareness.emotional_rating).label('emotional_rating_avg'), 
#        (models.Awareness.physical_rating).label('physical_rating_avg'), 
#        (models.Awareness.mental_rating).label('mental_rating_avg'), 
#        (models.Awareness.energetic_rating).label('energetic_rating_avg')).filter(models.Awareness.user_id == user_id) \
#                .filter(models.Awareness.creation_time >= start_date) \
#                .filter(models.Awareness.creation_time <= end_date)\
#                .group_by((models.Awareness.creation_time )).all()
	
           
#    if not awareness_entry:
#        return False
    
	#Rajni - IS final Rating being used anywhere?
#    a = []    
    #global final_rating
#    for i in awareness_entry:
#        a.append(list(i))
    
#    return awareness_entry


def get_all_ratings(access_tolken, start_date, end_date):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()

    if not access_entry:
        None

    user = access_entry.user_register
    user_id = user.id

    awareness_entry = db.session.query(func.date(models.Awareness.creation_time).label('creation_date_grouped'),
        func.avg(models.Awareness.physical_rating).label('physical_rating_avg'), 
        func.avg(models.Awareness.emotional_rating).label('emotional_rating_avg'),
        func.avg(models.Awareness.mental_rating).label('mental_rating_avg'),
        func.avg(models.Awareness.energetic_rating).label('energetic_rating_avg')).filter(models.Awareness.user_id == user_id) \
            .filter(models.Awareness.creation_time >= start_date) \
            .filter(models.Awareness.creation_time <= end_date) \
            .group_by(func.date(models.Awareness.creation_time)).all()
            
    if not awareness_entry:
        return False
    
    print awareness_entry
    return awareness_entry


def get_media_category(access_tolken):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()
    
    if not access_entry:
        return False
    
	user = access_entry.user_register
    user_id = user.id
    print user_id

    get_cateogry_table = db.session.query(models.media_categories.id,(models.media_categories.media_category).label('media_category'),
        (models.media_categories.em_impact).label('em_impact'), 
    	(models.media_categories.ph_impact).label('ph_impact'),
    	(models.media_categories.me_impact).label('me_impact'),
    	(models.media_categories.en_impact).label('en_impact')).filter(models.media_categories.id).all()
    if not get_cateogry_table:
        return False
    a = []
    for i in range(len(get_cateogry_table)):
        a.append(get_cateogry_table[i])

    b = []
    for i in a:
        b.append(list(i))
    
	cateogry = []
    for i in b:
        cateogry.append(i[1])
    
    global data
    data = []
    for i in b:
        data.append(i[1:])

    global columns
    columns = ['media_category', 'em_impact', 'ph_impact', 'me_impact', 'en_impact']
    global ratings
    ratings = views.printDatas()
    print printData()
    return get_cateogry_table


def printData():
    
    # print final_rating
    em_ratings = []
    ph_ratings = []
    me_ratings = []
    en_ratings = []
    em_ratings.append(ratings[0])
    ph_ratings.append(ratings[1])
    me_ratings.append(ratings[2])
    en_ratings.append(ratings[3]) 

    user_one = userContentGenerator.main(em_ratings, ph_ratings, me_ratings,en_ratings, data, columns)
    a = user_one.generate()
    # print a
    print '\n\n'
    print ''.join(a), " is very low to the user"
    output = views.get_the_final()
    b=  output.getMultimediaForContentGenerator() 
    global idGenerated
    idGenerated = []
    # print b
    for i in b:
        for j in a:
            if i[1] == j:
                print '\n'
                idGenerated.append(i[0])
                # return 'Operation finished'
 
    return True

def run_user_content_algorithm(access_tolken):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()
    
    if not access_entry:
        return False
    
	user = access_entry.user_register
    user_id = user.id
    print user_id

    media_entry_content = models.Multimedia.query.filter(models.Multimedia.media_id).all()
     
    if not media_entry_content:
        return False
    
    # print media_entry_content    
    return media_entry_content


def add_mm_user_content(access_tolken):
    get_all_media_ids = []
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()
    
    if not access_entry:
        return False
    
	user = access_entry.user_register
    user_id = user.id
    print user_id
    get_only = "no"
    
    media_entry = models.multimedia_user_content.query.filter(models.multimedia_user_content.favorites==get_only) \
                    .filter(models.multimedia_user_content.user_id==user_id).first()
    
    if media_entry is None:
        media_entry = models.multimedia_user_content()
        media_entry.user_id = user_id
    
    #using lambda reduce function to remove duplicate value and by the help of frozenset we return list of unique data
    mapping_ids = map(str,idGenerated)
    reducing_str =  list(frozenset(reduce((lambda a, b: a+b), mapping_ids)))
    media_entry.multimedia_ids  = ','.join(reducing_str)
    media_entry.favorites = "no"
    db.session.add(media_entry)
    db.session.commit()
    print media_entry
    return True


#THE CODE BELOW IS NO LONGER IN USE

def get_report_header(access_tolken, report_type):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()

    if not access_entry:
        None

    user = access_entry.user_register
    user_id = user.id
     
    report_header_entry = models.Reporting.query.filter(models.Reporting.report_type==report_type) \
                 .filter(models.Reporting.user_id==user_id).first()
                 
    print report_header_entry
    return report_header_entry


def get_report_data(access_tolken, start_date, end_date):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()

    if not access_entry:
        return None

    user = access_entry.user_register
    user_id = user.id

    reporting_entry = models.Reporting.query.filter(models.Reporting.user_id == user_id) \
                        .filter(models.Reporting.creation_time >= start_date) \
                        .filter(models.Reporting.creation_time <= end_date).all()
    return reporting_entry





def update_wesoeco_tier(tier, access_tolken):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()

    if not access_entry:
        return False

    user = access_entry.user_register

    user.wesoeco_tier = tier

    db.session.add(user)
    db.session.commit()
    return True


def get_wesoeco_tier(access_tolken):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()

    if not access_entry:
        return None

    user = access_entry.user_register

    return user.wesoeco_tier

def update_reporting(access_tolken, report_type, rating, activity=None):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()

    if not access_entry:
        return False

    user = access_entry.user_register
    user_id = user.id
    report_entry = models.Reporting()
    
    report_entry.user_id = user_id
    report_entry.report_type = report_type
    report_entry.rating = rating
    report_entry.activity = activity
    db.session.add(report_entry)
    db.session.commit()
    return True


def update_reporting_emotional(access_tolken, report_type, report_emotion_1, report_emotion_2, report_emotion_3, report_emotion_4, report_emotion_5):
    
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()
    if not access_entry:
        return False
    user = access_entry.user_register
    user_id = user.id
    print user_id
    
    
    report_entry = models.Reporting.query.filter(models.Reporting.report_type==report_type) \
                        .filter(models.Reporting.user_id==user_id).first()
    if not report_entry:
        return False
    report = report_entry.id
    id = report
    
    report_entry_emotional = models.ReportingEmotional.query.filter(models.ReportingEmotional.id==id) \
                        .filter(models.ReportingEmotional.user_id==user_id).first()
    if not report_entry_emotional:
        report_entry_emotional = models.ReportingEmotional()
    report_entry_emotional.id = id
    report_entry_emotional.user_id = user_id
    report_entry_emotional.report_emotion_1 = report_emotion_1
    report_entry_emotional.report_emotion_2 = report_emotion_2
    report_entry_emotional.report_emotion_3 = report_emotion_3
    report_entry_emotional.report_emotion_4 = report_emotion_4
    report_entry_emotional.report_emotion_5 = report_emotion_5
    db.session.add(report_entry_emotional)
    db.session.commit()
    return True


def update_reporting_physical(access_tolken, report_type, report_sleep, report_powernaps, report_vegetables, report_wholegrain, report_healthyprotein, report_fruits, report_organic_percent, report_water, report_healthyoils, report_calories, report_fav_activity1, report_fav_activity2, report_fav_activity3, report_fav_activity4, report_fav_activity5, report_fav_activity6):
    
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()
    if not access_entry:
        return False
    user = access_entry.user_register
    user_id = user.id
    print user_id
    
    
    report_entry = models.Reporting.query.filter(models.Reporting.report_type==report_type) \
                        .filter(models.Reporting.user_id==user_id).first()
    if not report_entry:
        return False
    report = report_entry.id
    id = report
    
    report_entry_physical = models.ReportingPhysical.query.filter(models.ReportingPhysical.id==id) \
                        .filter(models.ReportingPhysical.user_id==user_id).first()
    if not report_entry_physical:
        report_entry_physical = models.ReportingPhysical()
    report_entry_physical.id = id
    report_entry_physical.user_id = user_id
    report_entry_physical.report_sleep = report_sleep
    report_entry_physical.report_powernaps = report_powernaps
    report_entry_physical.report_vegetables = report_vegetables
    report_entry_physical.report_wholegrain = report_wholegrain
    report_entry_physical.report_healthyprotein = report_healthyprotein
    report_entry_physical.report_fruits = report_fruits
    report_entry_physical.report_organic_percent = report_organic_percent
    report_entry_physical.report_water = report_water
    report_entry_physical.report_healthyoils = report_healthyoils
    report_entry_physical.report_calories = report_calories
    report_entry_physical.report_fav_activity1 = report_fav_activity1
    report_entry_physical.report_fav_activity2 = report_fav_activity2
    report_entry_physical.report_fav_activity3 = report_fav_activity3
    report_entry_physical.report_fav_activity4 = report_fav_activity4
    report_entry_physical.report_fav_activity5 = report_fav_activity5
    report_entry_physical.report_fav_activity6 = report_fav_activity6
    db.session.add(report_entry_physical)
    db.session.commit()
    return True



