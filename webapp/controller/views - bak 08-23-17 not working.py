from flask import Flask
from flask import Blueprint, request, abort, jsonify
from webapp.controller import databasecontroller as dc
from flask.ext.cors import cross_origin
from datetime import date, timedelta
from collections import defaultdict
from flask_mail import Mail, Message
#import json as simplejson
import os, StringIO, io
#5/17/17 added by Ankit: import re to use regular expression
import re
from sqlalchemy import func

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
print (APP_ROOT)
#HTTP_ROOT_IP = 'http://52.42.218.102:80/images/'
#cwd = os.getcwd()
#print cwd+'/webapp/images'

wesoeco_view = Blueprint('wesoeco_view', __name__)


@wesoeco_view.route('/wesoeco/api/v1.0/register', methods=['POST'])
@cross_origin(origins='*')
def register():
    """
        request format:
            { 'email': <email_id>,
              'pass': <password>,
              'name': <name>}

        response format:
            { 'result': <SUCCESS/FAILURE>,
              'data': <additional dict or None>,
              'error': <error message> }
    """
    if (not request.json or 'email' not in request.json
        or 'pass' not in request.json or 'name' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': 'Registration Failed, This email exists already'}
    if dc.add_user(request.json['email'], request.json['pass'], request.json['name']):
        result['result'] = 'SUCCESS'
        result['error'] = ''
    
    print result
    return jsonify(result)

@wesoeco_view.route('/wesoeco/api/v1.0/external_user_register', methods=['POST'])
@cross_origin(origins='*')
def external_user_register():
    """
        request format:
            { 'external_user_id': <external_user_id>,
              'login_app': <login_app>,
              'name': <name>}

        response format:
            { 'result': <SUCCESS/FAILURE>,
              'data': <additional dict or None>,
              'error': <error message> }
    """
    if (not request.json or 'external_email_id' not in request.json
       or 'login_app' not in request.json or 'name' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    if dc.add_external_user(request.json['external_email_id'], request.json['login_app'], request.json['name']):
        result['result'] = 'SUCCESS'
    
    print result
    return jsonify(result)


@wesoeco_view.route('/wesoeco/api/v1.0/login', methods=['POST'])
@cross_origin(origins='*')
def login():
    print request.data
    if (not request.json or 'email' not in request.json
        or 'pass' not in request.json):
        return abort(400)

    access_tolken = dc.login(request.json['email'], request.json['pass'])
    result = {'result': 'FAILURE', 'data': {'access_tolken': access_tolken}, 'error': 'Login Failed'}
    if access_tolken:
        result['result'] = 'SUCCESS'
        result['error'] = ''
        
    print result
    return jsonify(result)


@wesoeco_view.route('/wesoeco/api/v1.0/external_user_login', methods=['POST'])
@cross_origin(origins='*')
def external_user_login():
    print request.data
    if (not request.json or 'external_email_id' not in request.json):
        return abort(400)

    access_tolken = dc.external_user_login(request.json['external_email_id'])
    result = {'result': 'FAILURE', 'data': {'access_tolken': access_tolken}, 'error': None}
    if access_tolken:
        result['result'] = 'SUCCESS'
        
    print result
    return jsonify(result)

"""
@wesoeco_view.route('/wesoeco/api/v1.0/external_user_login_check', methods=['POST'])
@cross_origin(origins='*')
def external_login_check():
    print request.data
    if (not request.json or 'external_user_id' not in request.json):
        return abort(400)

    user_exists = dc.external_user_login_check(request.json['external_user_id'])
    result = {'result': 'FAILURE', 'data': None, 'error': None}
    if user_exists:
        result['result'] = 'SUCCESS'
        
    print result
    return jsonify(result)
"""

@wesoeco_view.route('/wesoeco/api/v1.0/forgotpassword', methods=['POST'])
@cross_origin(origins='*')
def forgotpassword():
    print request.data
    if (not request.json or 'email_id' not in request.json):
        return abort(400)

    msg_sent = dc.forgotpassword(request.json['email_id'])
    result = {'result': 'FAILURE', 'data': {'msg_sent': msg_sent}, 'error': None}
    if msg_sent:
        result['result'] = 'SUCCESS'
        
    print result
    return jsonify(result)

@wesoeco_view.route('/wesoeco/api/v1.0/changepassword', methods=['POST'])
@cross_origin(origins='*')
def changepassword():
    if (not request.json or 'access_tolken' not in request.json 
            or 'cur_pass' not in request.json or 'new_pass' not in request.json):
            return abort(400)
  
    result = {'result': 'FAILURE', 'data': None, 'error': None}
    if dc.changepassword(request.json['access_tolken'], request.json['cur_pass'],  request.json['new_pass']):
        result['result'] = 'SUCCESS'
        
    print result
    return jsonify(result)


@wesoeco_view.route('/wesoeco/api/v1.0/update_setting', methods=['POST'])
@cross_origin(origins='*')
def update_setting():
    if (not request.json or 'access_tolken' not in request.json
        
        
        or 'setting_type' not in request.json or 'toggle' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    j = request.json
    if dc.update_setting(j['setting_type'], j['toggle'], j['access_tolken'], j.get('start_time'), j.get('end_time'), j.get('interval'), j.get('days_of_week'), j.get('tone')):
        result['result'] = 'SUCCESS'
    return jsonify(result)

@wesoeco_view.route('/wesoeco/api/v1.0/get_settings', methods=['POST'])
@cross_origin(origins='*')
def get_settings():
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    setting = dc.get_setting(request.json['access_tolken'], request.json['setting_type'])
    if not setting:
        return jsonify({'result': 'FAILURE', 'data': None, 'error': None})

    data = {'start_time': setting.start_time, 'end_time': setting.end_time,
            'interval': setting.interval, 'days_of_week': setting.days_of_week,
            'tone': setting.tone, 'toggle': setting.toggle}
    print data
    return jsonify({'result': 'SUCCESS', 'data': data, 'error': None})


@wesoeco_view.route('/wesoeco/api/v1.0/get_awareness', methods=['POST'])
@cross_origin(origins='*')
def get_awareness():
    if (not request.json or 'access_tolken' not in request.json 
            or 'start_date' not in request.json or 'end_date' not in request.json):
            return abort(400)

    awareness_data = dc.get_awareness(request.json['access_tolken'], request.json['start_date'], request.json['end_date'])
    if not awareness_data:
        return jsonify({'result': 'FAILURE', 'data': None, 'error': None})

    # 5/17/17 Ankit: using regular experssion pattern, to get the date from datetime field of database
    date_reg_exp = re.compile('\d{2}[-/]\d{2}[-/]\d{2}')
    
    awareness_results = []
    for awareness in awareness_data:
        #creation_date =  awareness.creation_time
        #getDate=date_reg_exp.findall(str(creation_date))
        a = {'creation_date': awareness.creation_date_grouped,
             'emotional_rating': awareness.emotional_rating_avg,
             'physical_rating': awareness.physical_rating_avg,
             'mental_rating': awareness.mental_rating_avg,
             'energetic_rating': awareness.energetic_rating_avg}
        
        #print a
        awareness_results.append(a)
        
    print awareness_results
    return jsonify({'result': 'SUCCESS', 'items': awareness_results, 'error': None})

@wesoeco_view.route('/wesoeco/api/v1.0/update_awareness', methods=['POST'])
@cross_origin(origins='*')
def update_awareness():
    """
    expect {'access_tolken': <access_tolken>}
    """
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    j = request.json

    if not dc.is_access_tolken_valid(j['access_tolken']):
        return jsonify(result)

   
    if dc.update_awareness(j['access_tolken'], j.get('emotional_rating'), j.get('physical_rating'), j.get('mental_rating'), j.get('energetic_rating')):
            result['result'] = 'SUCCESS'
    
    print result
    return jsonify(result)


@wesoeco_view.route('/wesoeco/api/v1.0/update_awareness_details', methods=['POST'])
@cross_origin(origins='*')
def update_awareness_details():
    
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    j = request.json

    if not dc.is_access_tolken_valid(j['access_tolken']):
        return jsonify(result)

    if dc.update_awareness_details(j['access_tolken'], j.get('emotional_state_details'), j.get('physical_state_details'), j.get('mental_state_details'), j.get('energetic_state_details')):
        result['concat'] = 'SUCCESS'
            
    print result
    return jsonify(result)


@wesoeco_view.route('/wesoeco/api/v1.0/get_user_media', methods=['POST'])
@cross_origin(origins='*')
def get_user_media():
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)
    
    #accesses the user_id and the associated multimedia_user_content record
    user_media = dc.get_user_media(request.json['access_tolken'])
    if not user_media:
        return jsonify({'result': 'FAILURE', 'data': None, 'error': None})
    
    print user_media
    print user_media.multimedia_ids
    print "LLLLLLLLLLLLLLL"
    data = {'user_id': user_media.user_id, 'created_date_time': user_media.created_date_time, 'multimedia_ids': user_media.multimedia_ids}
    print data
    return jsonify({'result': 'SUCCESS', 'data': data, 'error': None})


@wesoeco_view.route('/wesoeco/api/v1.0/get_multimedia', methods=['POST'])
@cross_origin(origins='*')
def get_multimedia():
    
    multimedia = dc.get_multimedia(request.json['media_id'])
    if not multimedia:
        return jsonify({'result': 'FAILURE', 'data': None, 'error': 'Media Record does not exist'})
    
    print "YYYYYY VIEWS CODE YYYYYYYYYYYYYYYYYYYYYYYY"
    print multimedia
    print multimedia.media_type
    print "ZZZZZZZZZZZZZZZZZZZZZ"
    data = {'media_id': multimedia.media_id, 'media_type': multimedia.media_type, 'media_file': multimedia.media_file, 'image_file': multimedia.image_file, 'image_title': multimedia.image_title, 'category': multimedia.category}
    print data
    return jsonify({'result': 'SUCCESS', 'data': data, 'error': None})


@wesoeco_view.route('/wesoeco/api/v1.0/multimedia_content_add', methods=['POST'])
@cross_origin(origins='*')
def multimedia_content_add():
	if (not request.json or 'media_type' not in request.json or 'media_file' not in request.json or 'image_title' not in request.json or 'image_file' not in request.json):
    		return abort(400)
		
	result = {'result': 'FAILURE', 'data': None, 'error': 'Content Adding Failed'}
    
	if dc.add_multimedia(request.json['media_type'], request.json['media_file'], request.json['image_title'], request.json['image_file']):
		result['result'] = 'SUCCESS'
		
    	return jsonify(result)

#Ankits Code Starts Here
@wesoeco_view.route('/wesoeco/api/v1.0/get_user_multimedia', methods=['POST'])
@cross_origin(origins='*')
def get_user_multimedia():
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)
    
    #accesses the user_id and the associated multimedia_user_content record
    user_media = dc.get_user_multimedia(request.json['access_tolken'])
    if not user_media:
        return jsonify({'result': 'FAILURE', 'data': None, 'error': None})
    
    print user_media
    print user_media.multimedia_ids
    print "Ankit LLLLLLLLLLLLLLL"
    data = {'user_id': user_media.user_id, 'created_date_time': user_media.created_date_time, 'multimedia_ids': user_media.multimedia_ids}
    print data
    return jsonify({'result': 'SUCCESS', 'data': data, 'error': None})


@wesoeco_view.route('/wesoeco/api/v1.0/get_multimedia_content', methods=['POST'])
@cross_origin(origins='*')
def get_multimedia_content():
    if (not request.json or 'access_tolken' not in request.json 
            or 'media_id' not in request.json):
            return abort(400)

    media_data_content = dc.get_multimedia_content(request.json['access_tolken'], request.json['media_id'])
    if not media_data_content:
        return jsonify({'result': 'FAILURE', 'data': None, 'error': None})
    
    meDataContent = [] 
    for ex in media_data_content:
        a = {
              'favorite': ex.favorite,
              'multimedia_ids': ex.media_id,
              'category': ex.category,
              'media_type': ex.media_type,
              'media_file': ex.media_file,
              'image_file': ex.image_file,
              'image_title': ex.image_title,
              'other': ex.other,
              'created_date_time': ex.creation_time
              }
        
        meDataContent.append(a)
        
    print a
    return jsonify({'result': 'SUCCESS', 'items': meDataContent, 'error': None})


@wesoeco_view.route('/wesoeco/api/v1.0/add_to_fav', methods=['POST'])
@cross_origin(origins='*')
def add_to_fav():
    """
    expect {'access_tolken': <access_tolken>}
    """
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    j = request.json

    # if not dc.is_access_tolken_valid(j['access_tolken']):
    #     return jsonify(result)

   
    if dc.add_to_fav(j['access_tolken'], j.get('favorite'), j.get('media_id')):
           result['result'] = 'SUCCESS'
    
    print result
    return jsonify(result)


@wesoeco_view.route('/wesoeco/api/v1.0/get_multimedia_favorite_content', methods=['POST'])
@cross_origin(origins='*')
def get_multimedia_favorite_content():
    if (not request.json or 'access_tolken' not in request.json 
            or 'media_id' not in request.json):
            return abort(400)


    media_data_content = dc.get_multimedia_favorite_content(request.json['access_tolken'], request.json['media_id'])
    if not media_data_content:
        return jsonify({'result': 'FAILURE', 'data': None, 'error': None})
    meDataContent = [] 
    for ex in media_data_content:
        a = {
              'multimedia_ids': ex.media_id,
              'category': ex.category,
              'media_type': ex.media_type,
              'media_file': ex.media_file,
              'image_file': ex.image_file,
              'image_title': ex.image_title,
              'other': ex.other,
              'created_date_time': ex.creation_time
              }
        # print a
        meDataContent.append(a)
        
    print a
    return jsonify({'result': 'SUCCESS', 'items': meDataContent, 'error': None})


#@wesoeco_view.route('/wesoeco/api/v1.0/remove_fav', methods=['POST'])
#@cross_origin(origins='*')
#def remove_fav():
#    """
#    expect {'access_tolken': <access_tolken>}
#    """
#    if (not request.json or 'access_tolken' not in request.json):
#        return abort(400)
#
#    result = {'result': 'FAILURE', 'data': None, 'error': None}
#    j = request.json
#
#    if dc.remove_fav(j['access_tolken'], j.get('favorite'), j.get('media_id')):
#           result['result'] = 'SUCCESS'
#    
#    print result
#    return jsonify(result)

#Ankits code ENDS HERE

#KULKARNIS CODE STARTS HERE 7/28/17

@wesoeco_view.route('/wesoeco/api/v1.0/validate_access_token', methods=['POST'])
@cross_origin(origins='*')
def validate_access_token():
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    j = request.json
    print j['access_tolken']
    result = {'result': 'FAILURE', 'data': None, 'error': None}

    if dc.validate_access_token(j['access_tolken']):
        result['result'] = 'SUCCESS'
        
    print result
    return jsonify(result)

#KULKARNIS CODE ENDS HERE 7/28/17


#THE CODE BELOW IS NO LONGER IN USE
@wesoeco_view.route('/wesoeco/api/v1.0/get_report_headers', methods=['POST'])
@cross_origin(origins='*') 
def get_report_headers():
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    report_header = dc.get_report_header(request.json['access_tolken'], request.json['report_type'])
    if not report_header:
        return jsonify({'result': 'FAILURE', 'data': None, 'error': None})

    data = {'id': report_header.id, 'user_id': report_header.user_id, 'report_type': report_header.report_type, 'rating': report_header.rating, 'activity': report_header.activity}
    print data
    return jsonify({'result': 'SUCCESS', 'data': data, 'error': None})



#<<<<======== EXERCISE MODULE(s) NEW, STARTS HERE ========>>>>
                #(Last Updated MAY 2017)

# Added on 5/23/17 - GET EXERCISE JOURNAL. This logic retrieve multiple exercise records for a USER and given DATE.
@wesoeco_view.route('/wesoeco/api/v1.0/get_exercise', methods=['POST'])
@cross_origin(origins='*')
def get_exercise():
    if (not request.json or 'access_tolken' not in request.json or 'exercise_date' not in request.json):
        return abort(400)

    exercise_data = dc.get_exercise(request.json['access_tolken'], request.json['exercise_date'])
    if not exercise_data:
        return jsonify({'result': 'FAILURE', 'data': None, 'error': None})
    
    exercise_results = []
    for exercise in exercise_data:
        e = {'exercise_id': exercise.exercise_id,
             'exercise_date': exercise.exercise_date,
             'exercise_desc': exercise.exercise_desc,
             'exercise_duration': exercise.exercise_duration,
             'calories_burned': exercise.calories_burned,
             'outdoor_ind': exercise.outdoor_ind
             }
        
        print e
        exercise_results.append(e)

    print jsonify(exercise_results)
    return jsonify({'result': 'SUCCESS', 'items': exercise_results, 'error': None})


#Added on 5/25/17 by Ankit, GET EXERCISE TYPE. The logic retrieves and display Users most recent exercise descriptins for ease of User data entry.
@wesoeco_view.route('/wesoeco/api/v1.0/get_exercise_type', methods=['POST'])
@cross_origin(origins='*')
def get_exercise_type():
    if (not request.json or 'access_tolken' not in request.json or 'exercise_date' not in request.json):
            return abort(400)

    #exercise_data = dc.get_exercise_type(request.json['access_tolken'], request.json['exercise_date'])
    exercise_data = dc.get_exercise_type(request.json['access_tolken'])
    if not exercise_data:
        return jsonify({'result': 'FAILURE', 'data': None, 'error': None})
    
    exercise_results = [] 
    for ex in exercise_data:
        a = {'exercise_desc': ex.exercise_desc}
        
        print a
        exercise_results.append(a)
    
    print jsonify(exercise_results)
    return jsonify({'result': 'SUCCESS', 'items': exercise_results, 'error': None})


# Added on 5/23/17 - UPDATE EXERCISE (accesses the NEW Exercise Database table)
@wesoeco_view.route('/wesoeco/api/v1.0/update_exercise', methods=['POST'])
@cross_origin(origins='*')
def update_exercise():
    """
    expect {'access_tolken': <access_tolken>}
    """
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    j = request.json

    if not dc.is_access_tolken_valid(j['access_tolken']):
        return jsonify(result)

   
    if dc.update_exercise(j['access_tolken'], j.get('exercise_date'), j.get('exercise_desc'), j.get('exercise_duration'), j.get('calories_burned'), j.get('outdoor_ind'), j.get('exercise_id')):
           result['result'] = 'SUCCESS'
    
    print result
    return jsonify(result)


# Added on 5/23/17 - DELETE EXERCISE NEW (accesses the new Exercise Database table)
@wesoeco_view.route('/wesoeco/api/v1.0/delete_exercise_new', methods=['POST'])
@cross_origin(origins='*')
def delete_exercise_new():
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    j = request.json
    
    if dc.delete_exercise_new(j['access_tolken'], j.get('exercise_date'), j.get('exercise_id')):
            result['result'] = 'SUCCESS'
            
    print result
    return jsonify(result)



# SLEEP JOURNAL code below:
#@wesoeco_view.route('/wesoeco/api/v1.0/get_sleep', methods=['POST'])
#@cross_origin(origins='*')
#def get_sleep():
#    if (not request.json or 'access_tolken' not in request.json):
#        return abort(400)
#
#    sleep = dc.get_sleep(request.json['access_tolken'], request.json['sleep_date'], request.json['sleep_id'])
#    if not sleep:
#        return jsonify({'result': 'FAILURE', 'data': None, 'error': None})
#
#    data = {'sleep_start_time': sleep.sleep_start_time, 'sleep_duration_hours': sleep.sleep_duration_hours, 'sleep_duration_minutes': sleep.sleep_duration_minutes}
#    print data
#    return jsonify({'result': 'SUCCESS', 'data': data, 'error': None})


# SLEEP JOURNAL code below - updated code as of 4/6/17 to retrieve multiple records.
@wesoeco_view.route('/wesoeco/api/v1.0/get_sleep', methods=['POST'])
@cross_origin(origins='*')
def get_sleep():
    if (not request.json or 'access_tolken' not in request.json or 'sleep_date' not in request.json):
        return abort(400)

    sleeplogs = dc.get_sleep(request.json['access_tolken'], request.json['sleep_date'])
    if not sleeplogs:
        return jsonify({'result': 'FAILURE', 'data': None, 'error': None})
    
    sleep_results = []
    for sleep in sleeplogs:
        s = {'sleep_id': sleep.sleep_id,
             'sleep_date': sleep.sleep_date,
             'sleep_start_time': sleep.sleep_start_time,
             'sleep_duration': sleep.sleep_duration,
             'sleep_duration_hours': sleep.sleep_duration_hours,
             'sleep_duration_minutes': sleep.sleep_duration_minutes
             }
        
        print s
        sleep_results.append(s)

    print jsonify(sleep_results)
    return jsonify({'result': 'SUCCESS', 'items': sleep_results, 'error': None})


@wesoeco_view.route('/wesoeco/api/v1.0/update_sleep', methods=['POST'])
@cross_origin(origins='*')
def update_sleep():
    """
    expect {'access_tolken': <access_tolken>}
    """
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    j = request.json

    if not dc.is_access_tolken_valid(j['access_tolken']):
        return jsonify(result)

   
    if dc.update_sleep(j['access_tolken'], j.get('sleep_date'), j.get('sleep_id'), j.get('sleep_start_time'), j.get('sleep_duration_hours'), j.get('sleep_duration_minutes')):
            result['result'] = 'SUCCESS'
   
    print result
    return jsonify(result)


@wesoeco_view.route('/wesoeco/api/v1.0/delete_sleep', methods=['POST'])
@cross_origin(origins='*')
def delete_sleep():
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    j = request.json
    
    if dc.delete_sleep(j['access_tolken'], j.get('sleep_date'), j.get('sleep_id')):
            result['result'] = 'SUCCESS'
            
    print result
    return jsonify(result)

# FOOD JOURNAL code below:
#@wesoeco_view.route('/wesoeco/api/v1.0/get_food', methods=['POST'])
#@cross_origin(origins='*')
#def get_food():
#    if (not request.json or 'access_tolken' not in request.json):
#        return abort(400)
#
#    food = dc.get_food(request.json['access_tolken'], request.json['food_date'], request.json['food_id'])
#    
#    if not food:
#        return jsonify({'result': 'FAILURE', 'data': '', 'error': 'Food Record does not exist'})
#
#    data = {'meal_type': food.meal_type, 'food_desc': food.food_desc, 'quantity_consumed': food.quantity_consumed, 'metric_of_measure': food.metric_of_measure,
#            'calories_consumed': food.calories_consumed, 'organic_ind': food.organic_ind}
#    print data
#    return jsonify({'result': 'SUCCESS', 'data': data, 'error': None})
#

# FOOD JOURNAL code below - updated code as of 4/6/17 to retrieve multiple records.
@wesoeco_view.route('/wesoeco/api/v1.0/get_food', methods=['POST'])
@cross_origin(origins='*')
def get_food():
    if (not request.json or 'access_tolken' not in request.json or 'food_date' not in request.json):
        return abort(400)

    foodlogs = dc.get_food(request.json['access_tolken'], request.json['food_date'])
    if not foodlogs:
        return jsonify({'result': 'FAILURE', 'data': None, 'error': None})
    
    food_results = []
    for food in foodlogs:
        f = {'meal_type': food.meal_type,
             'food_desc': food.food_desc,
             'quantity_consumed': food.quantity_consumed,
             'metric_of_measure': food.metric_of_measure,
             'calories_consumed': food.calories_consumed,
             'organic_ind': food.organic_ind
             }
        
        print f
        food_results.append(f)

    print jsonify(food_results)
    return jsonify({'result': 'SUCCESS', 'items': food_results, 'error': None})


@wesoeco_view.route('/wesoeco/api/v1.0/update_food', methods=['POST'])
@cross_origin(origins='*')
def update_food():
    """
    expect {'access_tolken': <access_tolken>}
    """
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    j = request.json

    if not dc.is_access_tolken_valid(j['access_tolken']):
        return jsonify(result)

   
    if dc.update_food(j['access_tolken'], j.get('food_date'), j.get('food_id'), j.get('meal_type'), j.get('food_desc'), j.get('quantity_consumed'), j.get('metric_of_measure'), j.get('calories_consumed'), j.get('organic_ind')):
            result['result'] = 'SUCCESS'
    
    print result
    return jsonify(result)


@wesoeco_view.route('/wesoeco/api/v1.0/delete_food', methods=['POST'])
@cross_origin(origins='*')
def delete_food():
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    j = request.json
    
    if dc.delete_food(j['access_tolken'], j.get('food_date'), j.get('food_id')):
            result['result'] = 'SUCCESS'
            
    print result
    return jsonify(result)


# 5/12/17 Kulkarni Get Food(USDA Integration) from new Table
# added usda_food_id column
@wesoeco_view.route('/wesoeco/api/v1.0/get_food_new', methods=['POST'])
@cross_origin(origins='*')
def get_food_new():
    if (not request.json or 'access_tolken' not in request.json or 'food_date' not in request.json):
        return abort(400)

    foodlogs = dc.get_food_new(request.json['access_tolken'], request.json['food_date'])
    if not foodlogs:
        return jsonify({'result': 'FAILURE', 'data': None, 'error': None})

    food_sequence = ['B', 'L', 'D', 'S', 'W']
     

    food_results = []
    sortList = []
    for food in foodlogs:
        f = {'food_id': food.food_id,
             'meal_type': food.meal_type,
             'food_desc': food.food_desc,
             'quantity_consumed': food.quantity_consumed,
             'metric_of_measure': food.metric_of_measure,
             'calories_consumed': food.calories_consumed,
             'organic_ind': food.organic_ind,
             'usda_food_id': food.usda_food_id
             }
                
        print sortList
        food_results.append(f)
        food_sorted_list = sorted(food_results, key=lambda v: food_sequence.index(v['meal_type']))

    print jsonify(food_results)
    print jsonify(food_sorted_list)
    return jsonify({'result': 'SUCCESS', 'items': food_sorted_list, 'error': None})


# 5/12/17 Kulkarni Add Food(USDA Integration) to new Table
# added usda_food_id field
@wesoeco_view.route('/wesoeco/api/v1.0/add_food_new', methods=['POST'])
@cross_origin(origins='*')
def add_food_new():
    """
    expect {'access_tolken': <access_tolken>}
    """
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    j = request.json

    if not dc.is_access_tolken_valid(j['access_tolken']):
        return jsonify(result)

    print j['access_tolken']
    print j.get('food_date')
    if dc.add_food_new(j['access_tolken'], j.get('food_date'), j.get('meal_type'), j.get('food_desc'),
                      j.get('quantity_consumed'), j.get('metric_of_measure'), j.get('calories_consumed'),
                      j.get('organic_ind'), j.get('usda_food_id')):
        result['result'] = 'SUCCESS'

    print result
    return jsonify({'result': 'SUCCESS', 'items': food_sorted_list, 'error': None})

# 5/12/17 Rajni - Combined both Add and Update Food functionalities (USDA Integration) code integrates to new Table
@wesoeco_view.route('/wesoeco/api/v1.0/update_food_new', methods=['POST'])
@cross_origin(origins='*')
def update_food_new():
    """
    expect {'access_tolken': <access_tolken>}
    """
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    j = request.json

    if not dc.is_access_tolken_valid(j['access_tolken']):
        return jsonify(result)

   
    if dc.update_food_new(j['access_tolken'], j.get('food_date'), j.get('food_id'), j.get('meal_type'), j.get('food_desc'), j.get('quantity_consumed'), j.get('metric_of_measure'), j.get('calories_consumed'), j.get('organic_ind'), j.get('usda_food_id')):
            #result = {'result': 'SUCCESS', 'data': j.get('food_id'), 'error': 'None'}
			result = {'result': 'SUCCESS', 'data': food_entries.food_id , 'error': 'None'}
             
    print result
    return jsonify(result)

@wesoeco_view.route('/wesoeco/api/v1.0/update_foodnutrients_consumed', methods=['POST'])
@cross_origin(origins='*')
def update_foodnutrients_consumed():
    
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    j = request.json

    if dc.update_foodnutrients_consumed(j['access_tolken'], j['food_id'], j['nutrientResultList']):
        result['result'] = 'SUCCESS'
            
    print result
    return jsonify(result)

# 5/12/17 Rajni Delete Food(USDA Integration) from new Table
@wesoeco_view.route('/wesoeco/api/v1.0/delete_food_new', methods=['POST'])
@cross_origin(origins='*')
def delete_food_new():
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    j = request.json
    
    if dc.delete_food_new(j['access_tolken'], j.get('food_date'), j.get('food_id')):
            result['result'] = 'SUCCESS'
            
    print result
    return jsonify(result)


# GRATITUDE JOURNAL code below:
@wesoeco_view.route('/wesoeco/api/v1.0/get_gratitude', methods=['POST'])
@cross_origin(origins='*')
def get_gratitude():
    if (not request.json or 'access_tolken' not in request.json or 'gratitude_date' not in request.json):
        return abort(400)

    j = request.json
    print(j["gratitude_date"])
    gratitudes = dc.get_gratitude(j['access_tolken'],j['gratitude_date'])

    if not gratitudes:
        return jsonify({'result': 'FAILURE', 'data': '', 'error': 'Gratitude Record does not exist'})
    
    gratitude_results = []
    for gratitude in gratitudes:
        #with open(gratitude.journal_image_address, 'rb') as inf:
        #    jpgdata = inf.read()
		gratitude_image_path = cwd + gratitude.journal_image_address
        http_image_link = HTTP_ROOT_IP + gratitude.journal_image_address
		print (http_image_link)

        d = {'gratitude_id': gratitude.gratitude_id,
             'gratitude_date': gratitude.gratitude_date,
             'gratitude_title': gratitude.gratitude_title,
             'journal_image_file_path': gratitude.journal_image_path,
             #'journal_image_address': jpgdata.encode('base64'),
			 'journal_image_address':http_image_link,
             'journal_text': gratitude.journal_text,
             }
        print d
        gratitude_results.append(d)
        #inf.close()

    return jsonify({'result': 'SUCCESS', 'items': gratitude_results, 'error': None})


@wesoeco_view.route('/wesoeco/api/v1.0/add_gratitudejournal', methods=['POST'])
def add_gratitudejournal():
    result = {'result': 'FAILURE', 'data': None, 'error': None}
	#updated by Kulkarni on 8/22/17.
    #target = os.path.join(APP_ROOT, 'images')
	target = os.path.join(cwd, 'webapp/images')
    print (target)

    if not os.path.isdir(target):
        os.mkdir(target)
    j = request.form.get

    for file in request.files.getlist('file'):
        filename = file.filename
        journal_image_address = "/".join([target, filename])
		#added by Kulkarni on 8/22/17 (1 line)
		journal_image_name =  filename
        print journal_image_address
        
    #if dc.add_gratitude(j("access_tolken"),j("gratitude_date"),j("gratitude_title"),journal_image_address,j("journal_text")):
	if dc.add_gratitude(j("access_tolken"),j("gratitude_date"),j("gratitude_title"),journal_image_name,j("journal_text")):
        print(journal_image_address)
        file.save(journal_image_address)
        result['result'] = 'SUCCESS'

    print result
    return jsonify(result)


@wesoeco_view.route('/wesoeco/api/v1.0/update_gratitude', methods=['POST'])
@cross_origin(origins='*')
def update_gratitude():
    result = {'result': 'FAILURE', 'data': None, 'error': None}
    target = os.path.join(APP_ROOT, 'images')
    print (target)
	static_path = '/images'

    if not os.path.isdir(target):
        os.mkdir(target)
        
    # This gets the parameters which are used to update the database and the AWS file system.
    j = request.form.get
    for file in request.files.getlist('file'):
        filename = file.filename
        print filename
        print 'image path above'

        journal_image_address = "/".join([target, filename])
		#2 lines added by Kulkarni on 8/22/17.
		journal_image_name = filename
        print (j("serverimagePath"))

        #if #dc.update_gratitude(j("access_tolken"),j("gratitude_id"),j("gratitude_date"),j("gratitude_title"),journal_image_address,j("journal_text"),j("serverimagePath")):
		if dc.update_gratitude(j("access_tolken"),j("gratitude_id"),j("gratitude_date"),j("gratitude_title"),journal_image_name,j("journal_text"),j("serverimagePath")):
            print(journal_image_address)
            if journal_image_address:
                file.save(journal_image_address)
            result['result'] = 'SUCCESS'

    print result
    return jsonify(result)

@wesoeco_view.route('/wesoeco/api/v1.0/update_gratitude', methods=['POST'])
@cross_origin(origins='*')
def update_gratitude():
    result = {'result': 'FAILURE', 'data': None, 'error': None}
    target = os.path.join(cwd, 'webapp/images')
    print (target)
    static_path = '/images'

    if not os.path.isdir(target):
        os.mkdir(target)
        
    # This gets the parameters which are used to update the database and the AWS file system.
    j = request.form.get
    for file in request.files.getlist('file'):
        filename = file.filename
        print filename
        print 'image path above'

        journal_image_address = "/".join([target, filename])
        journal_image_link = "/".join([static_path, filename])
        print (j("serverimagePath"))

        if dc.update_gratitude(j("access_tolken"),j("gratitude_id"),j("gratitude_date"),j("gratitude_title"),journal_image_link,j("journal_text"),j("serverimagePath")):
            print(journal_image_address)
            if journal_image_address:
                file.save(journal_image_address)
            result['result'] = 'SUCCESS'

    print result
    return jsonify(result)


@wesoeco_view.route('/wesoeco/api/v1.0/delete_gratitude', methods=['POST'])
@cross_origin(origins='*')
def delete_gratitude():
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    j = request.json
    
    if dc.delete_gratitude(j['access_tolken'], j.get('gratitude_date'), j.get('gratitude_id')):
            result['result'] = 'SUCCESS'
            
    print result
    return jsonify(result)


@wesoeco_view.route('/wesoeco/api/v1.0/update_feedback', methods=['POST'])
@cross_origin(origins='*')
def update_feedback():
    """
    expect {'access_tolken': <access_tolken>}
    """
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    j = request.json

    # if not dc.is_access_tolken_valid(j['access_tolken']):
    #     return jsonify(result)

   
    if dc.update_feedback(j['access_tolken'], j.get('feedback')):
           result['result'] = 'SUCCESS'
    
    print result
    return jsonify(result)

@wesoeco_view.route('/wesoeco/api/v1.0/update_favorite', methods=['POST'])
@cross_origin(origins='*')
def update_favorite():
    """
    expect {'access_tolken': <access_tolken>}
    """
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    j = request.json

    # if not dc.is_access_tolken_valid(j['access_tolken']):
    #     return jsonify(result)

   
    if dc.update_favorite(j['access_tolken'], j.get('fav_ids')):
           result['result'] = 'SUCCESS'
    
    print result
    return jsonify(result)

#Ankits code STARTS HERE 7/23/17
rating_data = []
#wesoeco_view = Blueprint('wesoeco_view', __name__)
@wesoeco_view.route('/wesoeco/api/v1.0/get_all_ratings', methods=['POST'])
@cross_origin(origins='*')
def get_all_ratings():
    '''
    step 1 function : used to get all the ratings from user awarnesss table
    and appending to global list called rating_data, global is used
    so we can call anywhere.
    '''
    if (not request.json or 'access_tolken' not in request.json 
            or 'start_date' not in request.json or 'end_date' not in request.json):
            return abort(400)

    awareness_data = dc.get_all_ratings(request.json['access_tolken'], request.json['start_date'], request.json['end_date'])
    if not awareness_data:
        return jsonify({'result': 'FAILURE', 'data': None, 'error': None})
    # using regular experssion pattern, to get the date from datetime field of database
    date_reg_exp = re.compile('\d{2}[-/]\d{2}[-/]\d{2}')
    awareness_results = []
    for awareness in awareness_data:
        creation_date =  awareness.creation_time
        getDate=date_reg_exp.findall(str(creation_date))
        a = {'creation_date' : getDate,
             'emotional_rating': awareness.emotional_rating_avg,
             'physical_rating': awareness.physical_rating_avg,
             'mental_rating': awareness.mental_rating_avg,
             'energetic_rating': awareness.energetic_rating_avg}

        awareness_results.append(a)
        #rating data is global you can call anywhere.
        rating_data.append(a)
    return jsonify({'result': 'SUCCESS', 'items': awareness_results, 'error': None})

@wesoeco_view.route('/wesoeco/api/v1.0/get_media_category', methods=['POST'])
@cross_origin(origins='*')
def get_media_category():
    if (not request.json or 'access_tolken' not in request.json  not in request.json):
        return abort(400)

    media_data_content = dc.get_media_category(request.json['access_tolken'])
    if not media_data_content:
        return jsonify({'result': 'FAILURE', 'data': None, 'error': None})
    
    meDataContent = [] 
    for ex in media_data_content:
        a = {
              'media_category': ex.media_category,
              'em_impact': ex.em_impact,
              'ph_impact': ex.ph_impact,
              'me_impact': ex.me_impact,
              'en_impact': ex.en_impact
              }
        
        meDataContent.append(a)

    return jsonify({'result': 'SUCCESS', 'items': meDataContent, 'error': None})

def printDatas():
    emotional_rating = []
    physical_rating = []
    mental_rating = []
    energetic_rating = []
    for i in range(len(rating_data)):
        emotional_rating.append(rating_data[i]['emotional_rating'])
        physical_rating.append(rating_data[i]['physical_rating'])
        mental_rating.append(rating_data[i]['mental_rating'])
        energetic_rating.append(rating_data[i]['energetic_rating'])
    return emotional_rating, physical_rating, mental_rating, energetic_rating

datas =[]
@wesoeco_view.route('/wesoeco/api/v1.0/get_render_multimedia', methods=['POST'])
@cross_origin(origins='*')
def get_render_multimedia():
    global myData
    if (not request.json or 'access_tolken' not in request.json  not in request.json):
      return abort(400)

    media_data_content = dc.get_render_multimedia(request.json['access_tolken'])
    if not media_data_content:
        return jsonify({'result': 'FAILURE', 'data': None, 'error': None})
    
    meDataContent = [] 
    for ex in media_data_content:
        a = {
              'multimedia_ids': ex.media_id,
              'category': ex.category,
              'media_type': ex.media_type,
              'media_file': ex.media_file,
              'image_file': ex.image_file,
              'image_title': ex.image_title,
              'other': ex.other,
              'created_date_time': ex.creation_time
              }
        
        meDataContent.append(a)

    global datas
   
    for i in meDataContent:
      a = i['multimedia_ids'],i['category']
      datas.append(a)
    return jsonify({'result': 'SUCCESS', 'items': meDataContent, 'error': None})

@wesoeco_view.route('/wesoeco/api/v1.0/add_multimedia_usercontent', methods=['POST'])
@cross_origin(origins='*')
def add_multimedia_usercontent():
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    j = request.json
   
    if dc.add_multimedia_usercontent(j['access_tolken']):
           result['result'] = 'SUCCESS'
    
    print result
    return jsonify(result)
class get_the_final():
  def getMultimediaForContentGenerator(self):
    return list(datas)

# end of Ankit's code

# THE CODE BELOW IS NO LONGER IN USE

@wesoeco_view.route('/wesoeco/api/v1.0/update_reporting_emotional', methods=['POST'])
@cross_origin(origins='*')
def update_reporting_emotional():
    
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    j = request.json

    if not dc.is_access_tolken_valid(j['access_tolken']):
        return jsonify(result)

    if dc.update_reporting_emotional(j['access_tolken'], j['report_type'], j.get('report_emotion_1'), j.get('report_emotion_2'), j.get('report_emotion_3'), j.get('report_emotion_4'), j.get('report_emotion_5')):
            result['result'] = 'SUCCESS'
    
    print result
    return jsonify(result)


@wesoeco_view.route('/wesoeco/api/v1.0/update_reporting_physical', methods=['POST'])
@cross_origin(origins='*')
def update_reporting_physical():
    
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    j = request.json

    if not dc.is_access_tolken_valid(j['access_tolken']):
        return jsonify(result)

    if dc.update_reporting_physical(j['access_tolken'], j['report_type'], j.get('report_sleep'), j.get('report_powernaps'), j.get('report_vegetables'), j.get('report_wholegrain'), j.get('report_healthyprotein'), j.get('report_fruits'), j.get('report_organic_percent'), j.get('report_water'), j.get('report_healthyoils'), j.get('report_calories'), j.get('report_fav_activity1'), j.get('report_fav_activity2'), j.get('report_fav_activity3'), j.get('report_fav_activity4'), j.get('report_fav_activity5'), j.get('report_fav_activity6')):
            result['result'] = 'SUCCESS'
            
    return jsonify(result)


@wesoeco_view.route('/wesoeco/api/v1.0/update_reporting', methods=['POST'])
@cross_origin(origins='*')
def update_reporting():
    """
    expect {'access_tolken': <access_tolken>, 'physical': {'rating': <rating>, 'activity': <activity>}}
    """
    if (not request.json or 'access_tolken' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    j = request.json

    report_types = ['physical', 'mental', 'emotional', 'energetic']

    if not dc.is_access_tolken_valid(j['access_tolken']):
        return jsonify(result)

    for report_type in report_types:
        if j.get(report_type):
            dc.update_reporting(j['access_tolken'], report_type, j.get(report_type).get('rating'),
                                activity=j.get(report_type).get('activity'))

            result['result'] = 'SUCCESS'
    return jsonify(result)

@wesoeco_view.route('/wesoeco/api/v1.0/get_reporting', methods=['POST'])
@cross_origin()
def get_reporting():
    def get_date_range(report):
        dates = sorted(set([r.creation_time.date() for r in report]))
        date_ranges = []

        prev_date = None
        for date in dates:
            if prev_date:
                delta_days = (date - prev_date).days
                for i in range(1, delta_days):
                    date_ranges.append(date + timedelta(days=i))
            prev_date = date
            date_ranges.append(prev_date)

        return [str(d) for d in date_ranges]

    if (not request.json or 'access_tolken' not in request.json
        or 'start_date' not in request.json or 'end_date' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    j = request.json
    d = defaultdict(lambda: defaultdict(list))

    s = j.get('start_date')
    start_date = date(int(s[0-3]), int(s[5-6]), int(s[8-9]))

    e = j.get('end_date')
    end_date = date(int(e[0-3]), int(e[5-6]), int(e[8-9]))
    end_date = end_date + timedelta(days=1)

    report = dc.get_report_data(j['access_tolken'], start_date, end_date)
    if not report:
        return jsonify(result)

    #report contains the list of awareness records from database
    dates = get_date_range(report)

    data_agg = defaultdict(lambda: defaultdict(int))
    data_count = defaultdict(lambda: defaultdict(int))
    for r in report:
        data_agg['Emotional'][str(r.creation_time.date())] += int(r.emotionalrating)
        data_count['Emotional'][str(r.creation_time.date())] += 1
        data_agg['Physical'][str(r.creation_time.date())] += int(r.physical_rating)
        data_count['Physical'][str(r.creation_time.date())] += 1
        data_agg['Mental'][str(r.creation_time.date())] += int(r.mental_rating)
        data_count['Mental'][str(r.creation_time.date())] += 1
        data_agg['Energetic'][str(r.creation_time.date())] += int(r.energetic_rating)
        data_count['Energetic'][str(r.creation_time.date())] += 1

    for report_type in data_count.keys():
        avg_list = []
        for d_l in dates:
            if data_count[report_type][d_l] > 0:
                avg_list.append((data_agg[report_type][d_l] / data_count[report_type][d_l]))
            else:
                avg_list.append(0)
        d[report_type]['dates'] = dates
        d[report_type]['avgs'] = avg_list

    result['result'] = 'SUCCESS'
    result['data'] = d

    print d
    return jsonify(result)

