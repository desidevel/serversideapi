import os
import ast
import time
import views
import uuid, random
import pandas as pd
from webapp import db
from sqlalchemy import func
import userContentGenerator 
from webapp.controller import models
from flask_mail import Mail, Message
def add_user(email_id, password, user_name, wesoeco_tier=None):
    user = models.UserRegister.query \
           .filter(models.UserRegister.email_id==email_id).first()
    if user:
        # User already exists
        return False
    
    user_registry = models.UserRegister.query \
           .filter(models.UserRegister.external_email_id==email_id).first()
    if not user_registry:
        # User already exists
        user_registry = models.UserRegister()
        
    #user_registry = models.UserRegister()
    user_registry.email_id = email_id
    user_registry.password = password
    user_registry.user_name = user_name
    user_registry.wesoeco_tier = wesoeco_tier

    db.session.add(user_registry)
    db.session.commit()
    return True


def add_external_user(external_email_id, login_app, user_name):
    #user = models.UserRegister.query \
    #       .filter((models.UserRegister.external_email_id==external_email_id) | (models.UserRegister.email_id==external_email_id))
        
    user = models.UserRegister.query \
           .filter(models.UserRegister.external_email_id==external_email_id).first()
    if user:
        # User already exists
        print "111111111111111111111111111111111111111111111111"
        return False
          
    external_user_registry = models.UserRegister.query \
           .filter(models.UserRegister.email_id==external_email_id).first()
    if not external_user_registry:
        # User already exists
        print "2222222222222222222222222222222222222222222222222"
        external_user_registry = models.UserRegister()
        
    print "Email Id NOT found for external user registry"
    external_user_registry.external_email_id = external_email_id
    external_user_registry.user_name = user_name
    external_user_registry.login_app = login_app
    db.session.add(external_user_registry)
    db.session.commit()
    return True


def login(email_id, password):
    user = models.UserRegister.query \
           .filter(models.UserRegister.email_id==email_id) \
           .filter(models.UserRegister.password==password).first()
    if not user:
        return None

    access_tolken = str(uuid.uuid4().int)
    access_tolken_entry = models.AccessTolken()
    access_tolken_entry.registration_id = user.id
    access_tolken_entry.access_tolken = access_tolken

    db.session.add(access_tolken_entry)
    db.session.commit()

    return access_tolken

def external_user_login(external_email_id):
    user = models.UserRegister.query \
           .filter(models.UserRegister.external_email_id==external_email_id).first()
    if not user:
        # User already exists
        print "44444444444444444444444444444444444444444444444444444444"
        return None

    print user
    print "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"
    access_tolken = str(uuid.uuid4().int)
    access_tolken_entry = models.AccessTolken()
    access_tolken_entry.registration_id = user.id
    access_tolken_entry.access_tolken = access_tolken

    db.session.add(access_tolken_entry)
    db.session.commit()

    return access_tolken

"""
def external_user_login_check(external_user_id):
    user = models.UserRegister.query \
           .filter(models.UserRegister.external_user_id==external_user_id).first()
    if not user:
        return None;

    access_tolken = str(uuid.uuid4().int)
    access_tolken_entry = models.AccessTolken()
    access_tolken_entry.registration_id = user.id
    access_tolken_entry.access_tolken = access_tolken

    db.session.add(access_tolken_entry)
    db.session.commit()

    return access_tolken
"""

def forgotpassword(email_id):
	email = models.UserRegister.query \
           .filter(models.UserRegister.email_id==email_id).first()
	print(email_id)

	if not email:
    	    return None
	try:
		#Random Password Generator
		alphabet = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		pw_length = 8
		mypw = ""

		for i in range(pw_length):
    			next_index = random.randrange(len(alphabet))
    			mypw = mypw + alphabet[next_index]
		pswd_check = models.UserRegister.query \
				.filter(models.UserRegister.email_id==email_id).first()
		pswd_check.password = mypw
		db.session.commit()

		print(mypw)
		
		#Mail functionality Starts Here
		msg_sent='Mail_Sent'
		msg = Message('Waliap Reset Password Notification ', sender = 'test.adstockglobal@gmail.com', recipients = ['brkulkarni89@gmail.com']) 
    		msg.body = 'Hello ,'+email_id+ '  ,\n  USER Name has requested to  generate new password ,  the generated  password mentioned below  \n ********\n ' +mypw+ '\n**************' 
		mail.send(msg)
		return msg_sent

	except Exception as e:
		return str(e)


def changepassword(access_tolken, current_password, new_password):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()

    if not access_entry:
        return False

    user = access_entry.user_register
    user_id = user.id
    password_entry = models.UserRegister.query.filter(models.UserRegister.user_id==user_id).first()
    
    if password_entry.password != current_password:
        print "current password is incorrect"
        return False
    password_entry.password = new_password
    db.session.add(password_entry)
    db.session.commit()
    return True

    
def is_access_tolken_valid(access_tolken):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()

    if not access_entry:
        return False
    return True
    
def get_userProfile(access_tolken):
    access_entry = models.AccessTolken.query.join(models.UserRegister) \
                    .filter(models.AccessTolken.access_tolken == access_tolken).first()

    if not access_entry:
        None

    # user = access_entry.userregister
    # user_id = user.id
    
    # pro = models.UserProfile.query.filter(models.UserProfile.name == 'Ankit').filter(models.UserProfile.age == 23)
    pro = db.session.query(models.UserProfile.id,(models.UserProfile.name).label('name'),
#         (models.MediaCategories.em_impact).label('em_impact'), 
    (models.UserProfile.age).label('age'),
    (models.UserProfile.gender).label('gender')).all()
            
    print pro
    return pro
