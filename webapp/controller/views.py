from flask import Flask
from flask import Blueprint, request, abort, jsonify
from webapp.controller import databasecontroller as dc
from flask_cors  import cross_origin
from datetime import date, timedelta
from collections import defaultdict
from flask_mail import Mail, Message
#import json as simplejson
import os, StringIO, io
#5/17/17 added by Ankit: import re to use regular expression
import re
from sqlalchemy import func

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
print (APP_ROOT)


wesoeco_view = Blueprint('wesoeco_view', __name__)


@wesoeco_view.route('/wesoeco/api/v1.0/register', methods=['POST'])
@cross_origin(origins='*')
def register():
    """
        request format:
            { 'email': <email_id>,
              'pass': <password>,
              'name': <name>}

        response format:
            { 'result': <SUCCESS/FAILURE>,
              'data': <additional dict or None>,
              'error': <error message> }
    """
    if (not request.json or 'email' not in request.json
        or 'pass' not in request.json or 'name' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': 'Registration Failed, This email exists already'}
    if dc.add_user(request.json['email'], request.json['pass'], request.json['name']):
        result['result'] = 'SUCCESS'
        result['error'] = ''
    
    print result
    return jsonify(result)

@wesoeco_view.route('/wesoeco/api/v1.0/external_user_register', methods=['POST'])
@cross_origin(origins='*')
def external_user_register():
    """
        request format:
            { 'external_user_id': <external_user_id>,
              'login_app': <login_app>,
              'name': <name>}

        response format:
            { 'result': <SUCCESS/FAILURE>,
              'data': <additional dict or None>,
              'error': <error message> }
    """
    if (not request.json or 'external_email_id' not in request.json
       or 'login_app' not in request.json or 'name' not in request.json):
        return abort(400)

    result = {'result': 'FAILURE', 'data': None, 'error': None}
    if dc.add_external_user(request.json['external_email_id'], request.json['login_app'], request.json['name']):
        result['result'] = 'SUCCESS'
    
    print result
    return jsonify(result)


@wesoeco_view.route('/wesoeco/api/v1.0/login', methods=['POST'])
@cross_origin(origins='*')
def login():
    print request.data
    if (not request.json or 'email' not in request.json
        or 'pass' not in request.json):
        return abort(400)

    access_tolken = dc.login(request.json['email'], request.json['pass'])
    result = {'result': 'FAILURE', 'data': {'access_tolken': access_tolken}, 'error': 'Login Failed'}
    if access_tolken:
        result['result'] = 'SUCCESS'
        result['error'] = ''
        
    print result
    return jsonify(result)


@wesoeco_view.route('/wesoeco/api/v1.0/external_user_login', methods=['POST'])
@cross_origin(origins='*')
def external_user_login():
    print request.data
    if (not request.json or 'external_email_id' not in request.json):
        return abort(400)

    access_tolken = dc.external_user_login(request.json['external_email_id'])
    result = {'result': 'FAILURE', 'data': {'access_tolken': access_tolken}, 'error': None}
    if access_tolken:
        result['result'] = 'SUCCESS'
        
    print result
    return jsonify(result)


@wesoeco_view.route('/wesoeco/api/v1.0/forgotpassword', methods=['POST'])
@cross_origin(origins='*')
def forgotpassword():
    print request.data
    if (not request.json or 'email_id' not in request.json):
        return abort(400)

    msg_sent = dc.forgotpassword(request.json['email_id'])
    result = {'result': 'FAILURE', 'data': {'msg_sent': msg_sent}, 'error': None}
    if msg_sent:
        result['result'] = 'SUCCESS'
        
    print result
    return jsonify(result)

@wesoeco_view.route('/wesoeco/api/v1.0/changepassword', methods=['POST'])
@cross_origin(origins='*')
def changepassword():
    if (not request.json or 'access_tolken' not in request.json 
            or 'cur_pass' not in request.json or 'new_pass' not in request.json):
            return abort(400)
  
    result = {'result': 'FAILURE', 'data': None, 'error': None}
    if dc.changepassword(request.json['access_tolken'], request.json['cur_pass'],  request.json['new_pass']):
        result['result'] = 'SUCCESS'
        
    print result
    return jsonify(result)



@wesoeco_view.route('/wesoeco/api/v1.0/get_user_profile', methods=['POST'])
@cross_origin(origins='*')
def get_user_profile():
   if (not request.json or 'access_tolken' not in request.json):
       return abort(400)

   profile = dc.get_userProfile(request.json['access_tolken'])
   if not profile:
       return jsonify({'result': 'FAILURE', 'data': None, 'error': None})

   for i in profile:
        mylist = {
           'name': i.name,
           'age': i.age,
           'gender': i.gender
        }
        res = mylist

   print res
   return jsonify({'result': 'SUCCESS', 'data': res, 'error': None})

    