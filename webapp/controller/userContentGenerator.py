# !/usr/bin/python

import numpy as np
import pandas as pd

class testUserData:

    def __init__(self, e_ratings=[], p_ratings=[], m_ratings=[], en_ratings=[], data=[], columns=[]):
        self.e_ratings = e_ratings
        self.p_ratings = p_ratings
        self.m_ratings = m_ratings
        self.en_ratings = en_ratings
        self.data = data
        self.columns = columns


        
    def getMinimumRating(self):
        '''
            Function takes the user rating ['physical', 'emotionally', 'mentally', 'energetically'],
            average the rating based on user input and return the minium rating among all.

            #important variables 
            all_rating_in_dict: store the average rating of user inputs, [Type: dictonary]
            min_rating : stores minimum rating value
            max_rating : stores maximum rating value
            
            
        '''
        
        emotional_rating = np.array(self.e_ratings)
        physical_rating   = np.array(self.p_ratings)
        mentally_rating = np.array(self.m_ratings)
        energetically_rating = np.array(self.en_ratings)

      
        # print '*Origianl Rating data from user input*\n'
        # print physical_rating
        # print emotional_rating
        # print mentally_rating
        # print energetically_rating

        # print '-------------------------------------------\n'
        remove_zero_from_physical = physical_rating[physical_rating!=0]
        remove_zero_from_emotional = emotional_rating[emotional_rating!=0]
        remove_zero_from_mentally = mentally_rating[mentally_rating!=0]
        remove_zero_from_energetically = energetically_rating[energetically_rating!=0]


        # print '*Removing zero if user has not given any ratings*\n'
        # print remove_zero_from_physical
        # print remove_zero_from_emotional
        # print remove_zero_from_mentally
        # print remove_zero_from_energetically
        # print '-------------------------------------------\n'
        

        #average the rating
        try:
            # print '*Average Rating*\n'
            ph_avg = float(sum(remove_zero_from_physical)) / float(len(remove_zero_from_physical))
            em_avg = float(sum(remove_zero_from_emotional)) / float(len(remove_zero_from_emotional))
            me_avg = float(sum(remove_zero_from_mentally)) / float(len(remove_zero_from_mentally))
            en_avg = float(sum(remove_zero_from_energetically)) / float(len(remove_zero_from_energetically))
        except ZeroDivisionError:
            print 'Paasing to another execution'
        # print'Physical Average Rating: ', ph_avg, '\nEmotional Average Rating: ', em_avg, '\nMentally Average Rating: ', me_avg, '\nEnergetically Average Rating: ', en_avg
        # print '-------------------------------------------\n'
        all_rating_in_dict = {'Physical rating': ph_avg,'Emotionally rating': em_avg,
                      'Mentally rating': me_avg,'Energetic rating': en_avg}
        min_value = min(all_rating_in_dict.itervalues())
        min_rating = [k for k in all_rating_in_dict if all_rating_in_dict[k] == min_value]
        del remove_zero_from_physical
        del remove_zero_from_emotional
        del remove_zero_from_mentally
        del remove_zero_from_energetically
        return  min_rating
    def emotional_low(self):
        df = pd.DataFrame(self.data,columns=self.columns)
        em_low = df[df['em_impact'] == 'high']
        em_low = em_low['media_category']
        category_gen = []
        for i in em_low:
            category_gen.append(i)
        return category_gen
    def physical_low(self):
        df = pd.DataFrame(self.data,columns=self.columns)
        ph_low = df[df['ph_impact'] == 'high']
        ph_low = ph_low['media_category']
        category_gen = []
        for i in ph_low:
            category_gen.append(i)
        return category_gen
    def mentally_low(self):
        df = pd.DataFrame(self.data,columns=self.columns)
        me_low = df[df['me_impact'] == 'high']
        me_low = me_low['media_category']
        category_gen = []
        for i in me_low:
            category_gen.append(i)
        return category_gen
    def energetically_low(self):
        df = pd.DataFrame(self.data,columns=self.columns)
        en_low = df[df['en_impact'] == 'high']
        en_low = en_low['media_category']
        category_gen = []
        for i in en_low:
            category_gen.append(i)
        return category_gen
    def emotional_energetic_physical_low(self):
        df = pd.DataFrame(self.data,columns=self.columns)
        em_low = df[df['em_impact'] == 'high']
        en_low = df[df['en_impact'] == 'high']
        ph_low = df[df['ph_impact'] == 'high']
        em_low = em_low['media_category']
        en_low = en_low['media_category']
        ph_low =  ph_low['media_category']
        
        
        categories = []
        for i in em_low:
            categories.append(i)
        for i in en_low:
            categories.append(i)
        for i in ph_low:
            categories.append(i)
        
        unique_categories = list(dict.fromkeys(categories))
        return unique_categories
    def mental_physical_low(self):
        df = pd.DataFrame(self.data,columns=self.columns)
        me_low = df[df['me_impact'] == 'high']
        ph_low = df[df['ph_impact'] == 'high']
        me_low =  me_low['media_category']
        ph_low =  ph_low['media_category']
        categories = []
        for i in me_low:
            categories.append(i)
        for i in ph_low:
            categories.append(i)
        unique_categories = list(dict.fromkeys(categories))
        print df
        return unique_categories
    def emotionally_mentally_low(self):
        df = pd.DataFrame(self.data,columns=self.columns)
        em_low = df[df['em_impact'] == 'high']
        me_low = df[df['me_impact'] == 'high']
        em_low =  em_low['media_category']
        me_low =  me_low['media_category']
        categories = []
        for i in em_low:
            categories.append(i)
        for i in me_low:
            categories.append(i)
        unique_categories = list(dict.fromkeys(categories))
        print df
        return unique_categories
    def energetic_mentally_low(self):
        df = pd.DataFrame(self.data,columns=self.columns)
        en_low = df[df['en_impact'] == 'high']
        me_low = df[df['me_impact'] == 'high']
        en_low =  en_low['media_category']
        me_low =  me_low['media_category']
        categories = []
        for i in en_low:
            categories.append(i)
        for i in me_low:
            categories.append(i)
        unique_categories = list(dict.fromkeys(categories))
        return unique_categories
    def energetic_mentally_physical(self):
        df = pd.DataFrame(self.data,columns=self.columns)
        en_low = df[df['en_impact'] == 'high']
        me_low = df[df['me_impact'] == 'high']
        ph_low = df[df['ph_impact'] == 'high']
        en_low =  en_low['media_category']
        me_low =  me_low['media_category']
        ph_low = ph_low['media_category']
        categories = []
        for i in en_low:
            categories.append(i)
        for i in me_low:
            categories.append(i)
        for i in ph_low:
            categories.append(i)
        unique_categories = list(dict.fromkeys(categories))
        return unique_categories
    def emotionally_physical_low(self):
        df = pd.DataFrame(self.data,columns=self.columns)
        em_low = df[df['em_impact'] == 'high']
        ph_low = df[df['ph_impact'] == 'high']
        em_low = em_low['media_category']
        ph_low = ph_low['media_category']
        categories = []
        for i in em_low:
            categories.append(i)
        for i in ph_low:
            categories.append(i)
        unique_categories = list(dict.fromkeys(categories))
        return unique_categories
    def emotionally_energetic_mentally_physical(self):
        df = pd.DataFrame(self.data,columns=self.columns)
        em_low = df[df['em_impact'] == 'high']
        en_low = df[df['en_impact'] == 'high']
        me_low = df[df['me_impact'] == 'high']
        ph_low = df[df['ph_impact'] == 'high']
        em_low = em_low['media_category']
        en_low =  en_low['media_category']
        me_low =  me_low['media_category']
        ph_low = ph_low['media_category']
        categories = []
        for i in em_low:
            categories.append(i)
        for i in en_low:
            categories.append(i)
        for i in me_low:
            categories.append(i)
        for i in ph_low:
            categories.append(i)
        unique_categories = list(dict.fromkeys(categories))
        return unique_categories
    def emotionally_energetic_low(self):
        df = pd.DataFrame(self.data,columns=self.columns)
        em_low = df[df['em_impact'] == 'high']
        en_low = df[df['en_impact'] == 'high']
        em_low = em_low['media_category']
        en_low =  en_low['media_category']
        categories = []
        for i in em_low:
            categories.append(i)
        for i in en_low:
            categories.append(i)
        unique_categories = list(dict.fromkeys(categories))
        return unique_categories
    def emotionally_energetic_mentally(self):
        df = pd.DataFrame(self.data,columns=self.columns)
        em_low = df[df['em_impact'] == 'high']
        en_low = df[df['en_impact'] == 'high']
        me_low = df[df['me_impact'] == 'high']
        em_low = em_low['media_category']
        en_low =  en_low['media_category']
        me_low =  me_low['media_category']
        categories = []
        for i in em_low:
            categories.append(i)
        for i in en_low:
            categories.append(i)
        for i in me_low:
            categories.append(i)
        unique_categories = list(dict.fromkeys(categories))
        return unique_categories
    def energetic_Physical_low(self):
        df = pd.DataFrame(self.data,columns=self.columns)
        en_low = df[df['en_impact'] == 'high']
        ph_low = df[df['ph_impact'] == 'high']
        en_low =  en_low['media_category']
        ph_low = ph_low['media_category']
        categories = []

        for i in en_low:
            categories.append(i)
        for i in ph_low:
            categories.append(i)
        unique_categories = list(dict.fromkeys(categories))
        return unique_categories
       
        

        

class main(testUserData):

    def generate(self):
        ''' Generate content based on minimum rating value'''
        min_rating = self.getMinimumRating()
        min_rating =  ' '.join(min_rating).strip()
        # print "\n\n------------------------------------------OUTPUT--------------------------------"
        if min_rating == 'Physical rating':
            return self.physical_low()
        if min_rating == 'Emotionally rating':
            return self.emotional_low()
            
        if min_rating == 'Mentally rating':
            return self.mentally_low()
        if min_rating == 'Energetic rating':
            return self.energetically_low()
       
        elif min_rating == 'Emotionally rating Energetic rating Physical rating':
            return self.emotional_energetic_physical_low()
        elif min_rating == 'Mentally rating Physical rating':
            return self.mental_physical_low()
        elif min_rating == 'Emotionally rating Mentally rating':
            return self.emotionally_mentally_low()
        elif min_rating == 'Energetic rating Mentally rating':
            return self.energetic_mentally_low()
        elif min_rating == 'Energetic rating Mentally rating Physical rating':
            return self.energetic_mentally_physical()
        elif min_rating == 'Emotionally rating Physical rating':
            return self.emotionally_physical_low()
        elif min_rating == 'Emotionally rating Energetic rating Mentally rating Physical rating':
            return self.emotionally_energetic_mentally_physical()
        elif min_rating == 'Emotionally rating Energetic rating':
            return self.emotionally_energetic_low()
        elif min_rating == 'Emotionally rating Energetic rating Mentally rating':
            return self.emotionally_energetic_mentally()
        elif min_rating == 'Energetic rating Physical rating':
            return self.energetic_Physical_low()
        else:
            return min_rating


'''DATA

media_category em_impact ph_impact me_impact en_impact
0   Motivational       high       low    medium      high
1        Fitness     medium      high    medium      high
2  Entertainment     medium       low    medium       low
3      Meditation    medium       low      high      high
4           Music      high    medium      high    medium
5          Speech      high       low      high    medium




'''
        

