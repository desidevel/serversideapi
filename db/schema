# 1. USER_REGISTER table **********************
CREATE TABLE `user_register` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `external_user_id` VARCHAR(25), #populated with Facebook of GooglePlus user id, if user logs in via these external sites
    `email_id` VARCHAR(50) COMMENT 'email for signin',
    `password` VARCHAR(50) COMMENT 'password',
    `user_name` VARCHAR(50) NOT NULL COMMENT 'email for signin',
    `login_app` VARCHAR(2), #populated with FB for Facebook login or GP for GooglePlus Login
    `wesoeco_tier` ENUM('1', '2', '3'),
    `creation_time` TIMESTAMP,
    `update_time` TIMESTAMP,
    CONSTRAINT pk_id PRIMARY KEY (`id`)
);

#Modified as of 1/11/2017, by Rajni
CREATE TABLE `userregister` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `external_email_id` VARCHAR(25), #populated with Facebook of GooglePlus user id, if user logs in via these external sites
    `email_id` VARCHAR(50) COMMENT 'email for signin',
    `password` VARCHAR(50) COMMENT 'password',
    `user_name` VARCHAR(50) NOT NULL COMMENT 'email for signin',
    `login_app` VARCHAR(2), #populated with FB for Facebook login or GP for GooglePlus Login
    `wesoeco_tier` ENUM('1', '2', '3'),
    `creation_time` TIMESTAMP,
    `update_time` TIMESTAMP DEFAULT now(),
    CONSTRAINT pk_id PRIMARY KEY (`id`)
);

ALTER TABLE user_register.ALTER COLUMN external_email_id VARCHAR(50);

# 2. ACCESS_TOLKEN table **********************
CREATE TABLE `access_tolken` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `registration_id` BIGINT NOT NULL,
    `access_tolken` VARCHAR(100) NOT NULL,
    # Add device id to associate with access_tolken
    `creation_time` TIMESTAMP,
    `update_time` TIMESTAMP DEFAULT now(),
    CONSTRAINT pk_access_id PRIMARY KEY (`id`),
    CONSTRAINT fk_registration_id FOREIGN KEY (`registration_id`) REFERENCES `userregister`(`id`)
);




# 3. SETTING table **********************
CREATE TABLE `setting` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `user_id` BIGINT NOT NULL,
    #`type` ENUM('breath', 'physical', 'mental', 'emotional', 'energetic'),
    `type` ENUM('breath', 'aware', 'improve', 'optimize'),
    # Convert 24 hours into minutes, eg: 8:00PM - 1200
    `start_time` INT(5),
    `end_time` INT(5),
    # Convert interval to minutes
    `interval` INT(5),
    `days_of_week` VARCHAR(10),
    # Store tone locations in file and associate index to them.
    `tone` INT(10),
    `toggle` BOOLEAN,
    #`physical` BOOLEAN,
    #`mental` BOOLEAN,
    #`emotional` BOOLEAN,
    #`energetic` BOOLEAN,
    `creation_time` TIMESTAMP,
    `update_time` TIMESTAMP DEFAULT now(),
    CONSTRAINT pk_setting_id PRIMARY KEY (`id`),
    CONSTRAINT fk_user_id FOREIGN KEY (`user_id`) REFERENCES `user_register`(`id`)
);

#4. The following 'awareness' and 'awareness_details' table(s) were added by Rajni on 10/15/16, to capture a User's "State of Being".
CREATE TABLE `awareness` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `user_id` BIGINT NOT NULL,
    `emotional_rating` INT,
    `physical_rating` INT,
    `mental_rating` INT,
    `energetic_rating` INT,
    `creation_time` TIMESTAMP,
    `update_time` TIMESTAMP DEFAULT now(),
    CONSTRAINT pk_awareness_id PRIMARY KEY (`id`),
    CONSTRAINT fk_awareness_uid FOREIGN KEY (`user_id`) REFERENCES `user_register`(`id`)
);

CREATE TABLE `awareness_details` (
    `id` BIGINT NOT NULL,
    `user_id` BIGINT NOT NULL,
    `emotional_state_details` VARCHAR(50),
    `physical_state_details` VARCHAR(50),
    `mental_state_details` VARCHAR(50),
    `energetic_state_details` VARCHAR(50),
    `creation_date_time` TIMESTAMP NOT NULL,
    `update_date_time` TIMESTAMP DEFAULT now(),
    CONSTRAINT pk_awareness_dets_id PRIMARY KEY (`id`),
    CONSTRAINT fk_awareness_dets_id FOREIGN KEY (`user_id`) REFERENCES `user_register`(`id`)
    #Moified By Rajni: as awareness records with same user_id's could be deleted due to FK constraint
    #CONSTRAINT fk_awareness_dets_id FOREIGN KEY (`user_id`) REFERENCES `awareness`(`user_id`)
);

#5. JOURNAL table(s) **********************
CREATE TABLE `journal_exercise` (
    `user_id` BIGINT NOT NULL,
    `exercise_date` DATE NOT NULL,
    `exercise_id` BIGINT NOT NULL,
    # For activity_start_time, convert 24 hours into minutes, eg: 8:00PM - 1200
    #`activity_start_time` INT(5) NOT NULL,
    `exercise_desc` varchar(30),
    `exercise_duration` INT,
    `calories_burned` INT,
    `outdoor_ind` ENUM('yes', 'no'),
    CONSTRAINT pk_journal_exercise PRIMARY KEY (user_id, exercise_date, exercise_id),
    CONSTRAINT fk_journal_exercise_uid FOREIGN KEY (`user_id`) REFERENCES `user_register`(`id`)
);

#5. JOURNAL table(s) **********************
CREATE TABLE `journal_exercise_new` (
    `exercise_id` BIGINT NOT NULL AUTO_INCREMENT,
    `user_id` BIGINT NOT NULL,
    `exercise_date` DATE NOT NULL,
    `exercise_desc` varchar(30),
    `exercise_duration` INT,
    `calories_burned` INT,
    `outdoor_ind` ENUM('yes', 'no'),
    CONSTRAINT pk_journal_exercise PRIMARY KEY (exercise_id),
    CONSTRAINT fk_journal_exercise_new_uid FOREIGN KEY (`user_id`) REFERENCES `user_register`(`id`)
);


CREATE TABLE `journal_sleep` (
    `user_id` BIGINT NOT NULL,
    `sleep_date` DATE NOT NULL,
    `sleep_id` BIGINT NOT NULL,
    # For activity_start_time, convert 24 hours into minutes, eg: 8:00PM - 1200
    `sleep_start_time` INT(5) NOT NULL,
    `sleep_duration` INT,
    `sleep_duration_hours` TINYINT,
    `sleep_duration_minutes` TINYINT,
    CONSTRAINT pk_track_sleep PRIMARY KEY (user_id,sleep_date,sleep_id),
    CONSTRAINT fk_track_sleep_uid FOREIGN KEY (`user_id`) REFERENCES `user_register`(`id`)
);

CREATE TABLE `journal_food` (
    `food_id` BIGINT NOT NULL,
    `user_id` BIGINT NOT NULL,
    `food_date` DATE NOT NULL,
    
    #'Breakfast', 'Lunch', 'Dinner', 'Snack', 'Water'
    `meal_type` ENUM('B', 'L', 'D', 'S', 'W'),
    `food_desc` varchar(30),
    `quantity_consumed` INT,
    `metric_of_measure` varchar(30),
    `calories_consumed` INT,
    `organic_ind` ENUM('yes', 'no'),
    CONSTRAINT pk_track_food PRIMARY KEY (user_id,food_date,food_id),
    CONSTRAINT fk_track_food_uid FOREIGN KEY (`user_id`) REFERENCES `user_register`(`id`)
);

CREATE TABLE `journal_food_new` (
  `food_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(11) NOT NULL,
  `food_date` date,
  `meal_type` enum('B','L','W','S','D') COMMENT 'Food Type Brakfast, Lunch, Water, Snacks, Dinner',
  `food_desc` varchar(200),
  `quantity_consumed` int(11),
  `metric_of_measure` varchar(50),
  `calories_consumed` bigint(11),
  `organic_ind` enum('yes','no'),
  `usda_food_id` varchar(20),
  CONSTRAINT pk_journal_food_new PRIMARY KEY (food_id),
  CONSTRAINT fk_journal_food_new_uid FOREIGN KEY (`user_id`) REFERENCES `user_register`(`id`)
)

CREATE TABLE `journal_gratitude` (
    `gratitude_id` BIGINT NOT NULL AUTO_INCREMENT,
    `user_id` BIGINT NOT NULL,
    `gratitude_date` DATE NOT NULL,
    `gratitude_title` varchar(30),
    `journal_image_address` varchar(300),
    `journal_text` varchar(300),
    CONSTRAINT pk_journal_gratitude PRIMARY KEY (gratitude_id),
    CONSTRAINT fk_journal_gratitude_uid FOREIGN KEY (`user_id`) REFERENCES `user_register`(`id`)
);


CREATE TABLE `journal_gratitude_rajni` (
    `user_id` BIGINT NOT NULL,
    `gratitude_date` DATE NOT NULL,
    `gratitude_id` BIGINT NOT NULL,
    `day_event_id` BIGINT NOT NULL, #so certain events for a day, can be logically grouped and displayed together
    `gratitude_type` varchar(2) NOT NULL, #not sure if we need this, P-for Photos, G-for Gratitude Entry, O-Other etc
    `jornal_text_or_image_address` varchar(300), #if gratitude_type = 'G', this can contain the journal entry, else it wil contain photo address
    CONSTRAINT pk_journal_gratitude PRIMARY KEY (user_id, gratitude_date, gratitude_id),
    CONSTRAINT fk_journal_gartitude_uid FOREIGN KEY (`user_id`) REFERENCES `user_register`(`id`)
);

ALTER TABLE multimedia MODIFY COLUMN image_title VARCHAR(100);

#6. MULTIMEDIA table(s) **********************
CREATE TABLE `multimedia` (
    `media_id` BIGINT NOT NULL AUTO_INCREMENT, # unique identifies and is the primary key,automatically generated
    `category` varchar(20), # this contains the category on the content that is being delivered
    `media_type` varchar(6), #video, audio, image, text - so program can bring up appropriate player to play video and audio files.
    `media_file` varchar(250), # contains file path/url and file name + file extension (.mp3, ,mpeg, .txt, .jpg, .png etc
    `image_file` varchar(250), #this contains the image path/url, and file name + file extension (.jpg, .png etc75
    `image_title` varchar(100),
    `other` varchar(25), #placeholder for any additional future fields
    `updated_by_user_id` BIGINT,
    `creation_time` TIMESTAMP,
    `update_time` TIMESTAMP DEFAULT now(),
    CONSTRAINT pk_id PRIMARY KEY (`media_id`),
    CONSTRAINT fk_updated_by_user_id FOREIGN KEY (`updated_by_user_id`) REFERENCES `user_register`(`id`)
);

ALTER TABLE multimedia_user_content MODIFY COLUMN multimedia_ids VARCHAR(100);

CREATE TABLE `multimedia_user_content` (
    `user_id` BIGINT NOT NULL,
    `created_date_time` TIMESTAMP NOT NULL, #do we want to limit the content generation page / day.
    `multimedia_ids` varchar(25) NOT NULL),  #contains multiple comma delimited id's OR point to id's in multimedia table - foreign key
    CONSTRAINT pk_media_user_content PRIMARY KEY (user_id, created_date_time),
     CONSTRAINT fk_media_user_content_uid FOREIGN KEY (`user_id`) REFERENCES `user_register`(`id`)
);

CREATE TABLE `multimedia_user_content` (
    `user_id` BIGINT NOT NULL,
    `created_date_time` TIMESTAMP NOT NULL,
    `multimedia_ids` varchar(25),
    'favorites' enum('yes','no'), 
    CONSTRAINT pk_media_user_content PRIMARY KEY (user_id, created_date_time),
    CONSTRAINT fk_media_user_content_uid FOREIGN KEY (`user_id`) REFERENCES `user_register`(`id`)
);




#6. OTHER table(s) **********************
CREATE TABLE `user_profile` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(15), #populated with Facebook of GooglePlus user id, if user logs in via these external sites
    `gender` VARCHAR(6) COMMENT 'email for signin',
    `age` INT(3) COMMENT 'password',
    `height` INT(3) NOT NULL COMMENT 'email for signin',
    `weight` INT(4), #populated with FB for Facebook login or GP for GooglePlus Login
    `activity-favs` VARCHAR(15),
    `music-favs` VARCHAR(15),
    `book-favs` VARCHAR(15),
    `humor-inspiration-favs` VARCHAR(15),
    `people-favs` VARCHAR(15),
    CONSTRAINT pk_id PRIMARY KEY (`id`)
);
CREATE TABLE `user_profile` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(15), #populated with Facebook of GooglePlus user id, if user logs in via these external sites
    `gender` VARCHAR(6) COMMENT 'email for signin',
    `age` INT(3) COMMENT 'password',
    CONSTRAINT pk_id PRIMARY KEY (`id`)
);







select user_id, awareness.id, emotional_rating, emotional_sob_details from awareness, awareness_details where awareness.id = awareness_details.id;



#SUBSTR(string,position)
#SUBSTR(string,position,length)
#SUBSTRING_INDEX(string, delimiter, count)

SELECT user_id, created_date_time, multimedia_ids
FROM multimedia_user_content AS a
WHERE created_date_time = (
    SELECT MAX(created_date_time)
    FROM multimedia_user_content AS b
    WHERE b.user_id = 1
);


Select id from multimedia
where id =
Select substing_index

SELECT multimedia_ids
FROM multimedia_user_content AS a
WHERE created_date_time = (
    SELECT MAX(created_date_time)
    FROM multimedia_user_content AS b
    WHERE b.user_id = 1
);


        
SELECT multimedia_ids, SUBSTRING_INDEX(multimedia_ids,',',1) AS part1,
select multimedia_ids SUBSTRING_INDEX(multimedia_ids,',',1) AS part1 from multimedia_user_content;

SELECT 
   SUBSTRING_INDEX(SUBSTRING_INDEX(multimedia_ids, ',', 1), ',', -1) AS first_id,
   If(  length(multimedia_ids) - length(replace(multimedia_ids, ',', ''))>1,
   SUBSTRING_INDEX(SUBSTR ING_INDEX(multimedia_ids, ',', 2), ',', -1) ,NULL) 
           as second_id,
   If(  length(multimedia_ids) - length(replace(multimedia_ids, ',', ''))>1,
   SUBSTRING_INDEX(SUBSTRING_INDEX(multimedia_ids, ',', 3), ',', -1) ,NULL) 
           as third_id,
   If(  length(multimedia_ids) - length(replace(multimedia_ids, ',', ''))>1,
   SUBSTRING_INDEX(SUBSTRING_INDEX(multimedia_ids, ',', 4), ',', -1) ,NULL) 
           as forth_id,
   If(  length(multimedia_ids) - length(replace(multimedia_ids, ',', ''))>1,
   SUBSTRING_INDEX(SUBSTRING_INDEX(multimedia_ids, ',', 5), ',', -1) ,NULL) 
           as fifth_id,
   SUBSTRING_INDEX(SUBSTRING_INDEX(multimedia_ids, ',', 20), ',', -1) AS last_id
from multimedia_user_content;

#select total # of id's in "multimedia_ids" column, excluding ',' delimiters.
SELECT multimedia_ids,
   CHAR_LENGTH(multimedia_ids) - CHAR_LENGTH(REPLACE(multimedia_ids, ',', '')) + 1 cnt
FROM multimedia_user_content
WHERE created_date_time = (
    SELECT MAX(created_date_time)
    FROM multimedia_user_content
    WHERE user_id = 1
);

select DATE(creation_time), avg(physical_rating), avg(emotional_rating), avg(mental_rating), avg(energetic_rating) from awareness where user_id = 1 and creation_time between '2017-01-07' and '2017-02-09' group by DATE(creation_time);


insert into reporting (user_id, report_type, rating, activity, creation_time, update_time)
values(1,'physical',3,'test',now(),now());
insert into reporting (user_id, report_type, rating, activity, creation_time, update_time)
values(1,'emotional',3,'testEM',now(),now());
insert into reporting (user_id, report_type, rating, activity, creation_time, update_time)
values(1,'mental',5,'testME',now(),now());
insert into reporting (user_id, report_type, rating, activity, creation_time, update_time)
values(1,'energetic',4,'testEN',now(),now());

 update reporting
    set creation_time = '2016-11-23 11:30:00', update_time = '2016-11-23 11:30:00' 
    where id > 12;
    
    update reporting
     set creation_time = '2016-11-23 10:30:00' where id > 8;

CREATE TABLE `journal_sleep` (
    `user_id` BIGINT NOT NULL,
    `sleep_date` DATE NOT NULL,
    `sleep_id` BIGINT NOT NULL,
    # For activity_start_time, convert 24 hours into minutes, eg: 8:00PM - 1200
    `sleep_start_time` INT(5) NOT NULL,
    `sleep_duration` INT,
    `sleep_duration_hours` TINYINT,
    `sleep_duration_minutes` TINYINT,
    CONSTRAINT pk_track_sleep PRIMARY KEY (user_id,sleep_date,sleep_id),
    CONSTRAINT fk_track_sleep_uid FOREIGN KEY (`user_id`) REFERENCES `user_register`(`id`)
);


CREATE TABLE `journal_food_nutrients` (
  `food_id` BIGINT(11)  COMMENT 'Foreign key to journal_food_new',
  `usda_nutrient_id` INT(11)  COMMENT 'Nutrient_id from usda API',
  `usda_nutrient_name` VARCHAR(100) DEFAULT NULL COMMENT 'nutrient name from usda nutrient list',
  #`quantity_entered` INT(11) DEFAULT NULL COMMENT 'quantity consumed',
  `nutrient_value_consumed` DOUBLE DEFAULT NULL COMMENT 'amount_consumed is user entered quantity * nutrient value',
  CONSTRAINT pk_journal_nutrient PRIMARY KEY (food_id, usda_nutrient_id),
  CONSTRAINT fk_journal_nutrient_uid FOREIGN KEY (`food_id`) REFERENCES `journal_food_new`(`food_id`))
